import "react-native-gesture-handler";
import React, { useEffect, useMemo, useState } from 'react'
import { LogBox } from "react-native";
import { Provider as PaperProvider } from "react-native-paper";
import AuthContext from "./src/context/AuthContext";
import { getTokenApi, removeTokenApi, setTokenApi } from "./src/api/token";
import { Auth as AuthScreen } from "./src/screens/Auth";
import { NavigationStack } from "./src/navigator/NavigationStack";
import { getCityApi, setCityApi } from "./src/api/city";


//Ignora warnings
LogBox.ignoreLogs([
  "ViewPropTypes will be removed",
  "ColorPropType will be removed",
]);


const App = () => {
  const [auth, setAuth] = useState<any>(false);
  const [startScreen, setStartScreen] = useState("loginScreen");
  
  //Inicializa el auth
  useEffect(() => {
    (async () =>{
      //const gg= await removeTokenApi();
      const iduser = await getTokenApi();
      const idcity = await getCityApi();
      
      if(iduser){
        setAuth({
          iduser:parseInt(iduser),
          idcity:parseInt(idcity)
        })
      }else{
        setAuth(false);
      }
    })()
  }, []);

  //Llama la respuesta del login desde el LoginScreen
  const login=(user:any)=>{
    setTokenApi(parseInt(user.result[0].IdCatUser));
    setCityApi(parseInt(user.result[0].IdCatCiudad));

    setAuth({
      iduser:parseInt(user.result[0].IdCatUser),
      idcity:parseInt(user.result[0].IdCatCiudad)
    })
  }

  //Cierra sesion y remueve el token
  const logout=()=>{ 
    if(auth){
      removeTokenApi();
      setAuth(false)
    }
  }

  const recovery=(screen:any)=>{
    setStartScreen(screen);
    //console.log(screen);   
  }

  //Si alguno de estas variables cambia, se refresca desde el AuthContext
  const authData=useMemo(()=>({ auth,login,logout,recovery}),[auth]);
  console.log(authData);

  //Setea el auth
  //if(auth === undefined) return null;


  return (
    //Pantalla principal a iniciar
      <AuthContext.Provider value={authData} >
      <PaperProvider>
        {auth ? <NavigationStack/> : <AuthScreen startScreen={startScreen} ></AuthScreen>}
      </PaperProvider>
    </AuthContext.Provider>
  );
};


export default App;
