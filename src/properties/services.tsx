import { API_URL } from "../properties/constants";

//Inicio de sesion
export async function loginUser(form: any) {
  try {
    const url = `${API_URL}/api/login`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Registro de usuarios
export async function registerUser(form: any) {
  try {
    const url = `${API_URL}/api/registerUser`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Get a catalogos de ciudades
export async function getCitys() {
  try {
    const url = `${API_URL}/api/getCitys`;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Get de usuario por id
export async function getUserById(userid: any) {
  try {
    const url = `${API_URL}/api/getUserById/` + userid;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Get de productos por organization
export async function getProducts(idciudad: string) {
  try {
    const url = `${API_URL}/api/getProducts/` + idciudad;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Get de producto by Id
export async function getProductById(idproduct: string, idcity: any) {
  try {
    const url = `${API_URL}/api/getProductById/` + idproduct + "/" + idcity;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Registro de direcciones
export async function registerAddress(form: any) {
  try {
    const url = `${API_URL}/api/registerAddress`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Get de direccion por idCiudad
export async function getAddressByCity(idciudad: any) {
  try {
    const url = `${API_URL}/api/getAddressByCity/` + idciudad;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Registro de carrito
export async function registerCart(form: any) {
  try {
    const url = `${API_URL}/api/registerCart`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };

    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Get de pedidos por usuario
export async function getOrdersByUser(userid: any) {
  try {
    //const url = `${API_URL}/api/getOrdersByUser/`+userid;
    const url = `${API_URL}/api/getOrdersGroupByUser/` + userid;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Get
export async function getComparePriceCart(form: any) {
  try {
    const url = `${API_URL}/api/getComparePriceCart`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Get de producto by Id
export async function validateAddress(idAddressCity: any, selectCity: any) {
  try {
    const url =
      `${API_URL}/api/validateAddress/` + idAddressCity + "/" + selectCity;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//Get de centro de venta by diudad
export async function getCentroVenta(cityId: any) {
  try {
    const url = `${API_URL}/api/getCentroVenta/` + cityId;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Reestrablecer password
export async function restorePassowrd(form: any) {
  try {
    const url = `${API_URL}/api/sendMail`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

export async function getAclaraciones(cityId: any) {
  try {
    const url = `${API_URL}/api/getAclaraciones/` + cityId;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

export async function deleteUserApp(form: any) {
  try {
    const url = `${API_URL}/api/eliminarUsuarioApp`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//Subida file
export async function solicitarFactura(
  idVenta: number,
  file: any,
  IdCatUsoCFDI: string,
  IdCatMetododePago: string
) {
  try {
    const url = `${API_URL}/api/solicitarFactura/${idVenta}`;

    const formData = new FormData();
    formData.append("file", file);
    formData.append("IdCatUsoCFDI", IdCatUsoCFDI);
    formData.append("IdCatMetodoPago", IdCatMetododePago);

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*",
        "Content-Type": "multipart/form-data",
      },
      body: formData,
    });

    const result = await response.json();
    return result;
  } catch (error) {
    return error;
  }
}

//
export async function getCatMetPago() {
  try {
    const url = `${API_URL}/api/getCatMetPago`;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

//
export async function getCatUsoCFDI() {
  try {
    const url = `${API_URL}/api/getCatUsoCFDI`;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}
