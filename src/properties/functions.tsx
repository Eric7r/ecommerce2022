export function sortArrayByDate(array:any){
    return array.sort(function (a:any,b:any){
        return new Date(b.date) - new Date(a.date);
    });

}