export const API_URL = "https://ecommercerestprueba.kowi.com.mx";
export const API_URL_IMAGES ="https://ecommerce.kowi.com.mx/images/products/";
export const TOKEN = "token";
export const IDUSER = "iduser";
export const IDCITY = "idcity";
export const CART = "cart";
export const SEARCH_HISTORY = "search-history";
