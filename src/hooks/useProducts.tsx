//import React from 'react'
import { useEffect, useState } from 'react';
import baseURL from '../api/baseURL';
import { ProductResponse } from '../interfaces/productsInterface'
import useAuth from './useAuth';

interface ProductsState{
    productosCategory1:ProductResponse[]
    productosCategory2:ProductResponse[]
    productosCategory3:ProductResponse[]
    productosCategory4:ProductResponse[]
    productosCategory5:ProductResponse[]
    productosCategory6:ProductResponse[]
    productosCategory7:ProductResponse[]
    productosCategory8:ProductResponse[]
    productosCategory9:ProductResponse[]
    productosCategory10:ProductResponse[]

}

export const useProducts = () => {

    const {auth}=useAuth();
    
    const [isLoading, setIsLoading] = useState(true);
    const [productsState,setProductsState] = useState<ProductsState> ({
        productosCategory1:[],
        productosCategory2:[],
        productosCategory3:[],
        productosCategory4:[],
        productosCategory5:[],
        productosCategory6:[],
        productosCategory7:[],
        productosCategory8:[],
        productosCategory9:[],
        productosCategory10:[]
    });

    const getProducts = async ()=> {
        const productosPromise = baseURL.get<any>('/api/getProductsByCategory/1/'+auth?.idcity);
        const productosPromise2 = baseURL.get<any>('/api/getProductsByCategory/2/'+auth?.idcity);
        const productosPromise3 = baseURL.get<any>('/api/getProductsByCategory/3/'+auth?.idcity);
        const productosPromise4 = baseURL.get<any>('/api/getProductsByCategory/4/'+auth?.idcity);
        const productosPromise5 = baseURL.get<any>('/api/getProductsByCategory/5/'+auth?.idcity);
        const productosPromise6 = baseURL.get<any>('/api/getProductsByCategory/6/'+auth?.idcity);
        const productosPromise7 = baseURL.get<any>('/api/getProductsByCategory/7/'+auth?.idcity);
        const productosPromise8 = baseURL.get<any>('/api/getProductsByCategory/8/'+auth?.idcity);
        const productosPromise9 = baseURL.get<any>('/api/getProductsByCategory/9/'+auth?.idcity);
        const productosPromise10 = baseURL.get<any>('/api/getProductsByCategory/10/'+auth?.idcity);


        const response= await Promise.all([
            productosPromise,
            productosPromise2,
            productosPromise3,
            productosPromise4,
            productosPromise5,
            productosPromise6,
            productosPromise7,
            productosPromise8,
            productosPromise9,
            productosPromise10
        ])

        setProductsState({
            productosCategory1: response[0].data,
            productosCategory2: response[1].data,
            productosCategory3: response[2].data,
            productosCategory4: response[3].data,
            productosCategory5: response[4].data,
            productosCategory6: response[5].data,
            productosCategory7: response[6].data,
            productosCategory8: response[7].data,
            productosCategory9: response[8].data,
            productosCategory10: response[9].data,
        })

        setIsLoading(false);
    }

    useEffect(() => {
        getProducts();
    }, [])

    const reload = async ()=> {
        await getProducts();
        return true;
    }
    
    //Exponer variables
    return{
        ...productsState,
        isLoading,
        reload
    }
 
}
