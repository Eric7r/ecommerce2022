import axios from "axios";

const baseURL=axios.create({
    baseURL:'https://ecommercerestprueba.kowi.com.mx',
});

export default baseURL;