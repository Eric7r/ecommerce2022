import { API_URL } from "../properties/constants";

export async function getAddressApi(auth: any) {

  try {
    const url = `${API_URL}/api/addressByUser/${auth?.iduser}`;
    fetch(url).then((response) => response.json());
    const response = await fetch(url);
    const result = await response.json();
    return result;
    
  } catch (error) {
    return error;
  }
}

export async function deleteAddressApi(idAddress: any) {
  try {

    const url = `${API_URL}/api/deleteAddress/${idAddress}`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: idAddress,
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }

}