import AsyncStorage from "@react-native-async-storage/async-storage";
import { CART } from "../properties/constants";
import {size,map,filter} from "lodash";
import useAuth from "../hooks/useAuth";



export async function deleteAllCart() {
    await AsyncStorage.removeItem(CART);
}


export async function getProductCartApi(iduser:any) {
    
    //await AsyncStorage.removeItem(CART);
    try {
        const cart= await AsyncStorage.getItem(CART);

        //Valida si esta vacio el carrito
        if(!cart){
            return [];
        }else{
            //Filtra por usuario la lista de productos del carrito
           const newCart= JSON.parse(cart);
           const filterCart= newCart.filter( (item: any) => item.iduser==iduser);
           //console.log(filterCart);
           return filterCart;
        }

    } catch (error) {
        return null;
    }
}

export async function addProductCartApi(idProduct:any,quantity:any, tipo:any,iduser:any,price:any) {
    
    try {
        const cart= await getProductCartApi(iduser);
        if(!cart) throw "Error al obtener carrito"

        if(size(cart)===0){
            cart.push({
                idProduct,
                quantity,
                tipo,
                iduser,
                price
            })
        }else{
            let found=false;
            map(cart,(product:any)=>{
                
                if(product.idProduct===idProduct){
                    product.quantity+=quantity;
                    product.tipo=tipo;
                    product.iduser=iduser;
                    product.price=price;
                    found=true;
                    return product;
                }
            });

            if(!found){
                cart.push({
                    idProduct,
                    quantity,
                    tipo,
                    iduser,
                    price
                });
            }
        }

        await AsyncStorage.setItem(CART,JSON.stringify(cart));
        //console.log(cart);
        
        return true;
    } catch (error) {
        
    }
}

export async function deleteProductCartApi(idProduct:any,iduser:any) {
    
    try {
        const cart= await getProductCartApi(iduser);
        const newCart = filter(cart,(product:any)=>{
            return product.idProduct!=idProduct
        });
        
        await AsyncStorage.setItem(CART, JSON.stringify(newCart));
        return true;
    } catch (error) {
        return null;
    }
}

export async function increaseProductCartApi(idProduct:any,iduser:any) {
    try {
        const cart= await getProductCartApi(iduser);
        map(cart,(product:any)=>{
           if(product.idProduct==idProduct){
            return (product.quantity+=1)
           }
           
        })

        await AsyncStorage.setItem(CART,JSON.stringify(cart));
        return true;
        
    } catch (error) {
        return null;
    }
}

export async function decreaseProductCartApi(idProduct:any,iduser:any) {
    let isDelete=false;

    try {
        const cart= await getProductCartApi(iduser);
        map(cart,(product:any)=>{
           if(product.idProduct==idProduct){
            if(product.quantity==1){
                isDelete=true;
                return null;
            }else{
                return (product.quantity -= 1);
            }
           }
        });

        if(isDelete){
            await deleteProductCartApi(idProduct,iduser)
        }else{
            await AsyncStorage.setItem(CART,JSON.stringify(cart));
        }

        return true;
        
    } catch (error) {
        return null;
    }
}
