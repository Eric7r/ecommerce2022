import { API_URL } from "../properties/constants";

export async function isFavoriteApi(idProduct:any){

    try {
        const url = `${API_URL}/api/getFavorites/${idProduct}`;
        const response = await fetch(url);
        const result= await response.json();
        return result;
    } catch (error) {
        return error;
        
    }
}