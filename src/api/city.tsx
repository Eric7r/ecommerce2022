import AsyncStorage from '@react-native-async-storage/async-storage';
import { IDCITY } from '../properties/constants';

//Setea la ciudad
export async function setCityApi(idcity:any){
    try {
      await AsyncStorage.setItem(IDCITY,JSON.stringify(idcity));
      return true;
    } catch (error) {
      return null;
    }
}
//Obtiene la ciudad
export async function getCityApi(){
    try {
      const idcity= await AsyncStorage.getItem(IDCITY);
      return idcity;
    } catch (error) {
      return null;
    }
}
//Elimina la ciudad
export async function removeCityApi(){
    try {
        await AsyncStorage.removeItem(IDCITY);
        return true;
      } catch (error) {
        return null;
      }
  }