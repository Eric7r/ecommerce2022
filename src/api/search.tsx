//Se guarda el local storage del historial de busquedas
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL, SEARCH_HISTORY } from "../properties/constants";
import { size } from 'lodash';
import { sortArrayByDate } from "../properties/functions";

export async function getSearchHistoryApi() {

    //Borrar historial de bara de busqueda
    //await AsyncStorage.removeItem(SEARCH_HISTORY);

    try {
        const history= await AsyncStorage.getItem(SEARCH_HISTORY);
        if(!history) return [];
        return sortArrayByDate(JSON.parse(history));
        
    } catch (error) {
        console.log(error);
        
    }
    return [];
    
}

export async function updateSearchHistoryApi(search:any) {
    const history:any= await getSearchHistoryApi();
  
    if(size(history)>5) history.pop();

    history.push({
        search,
        date: new Date(),
    });
    
    await AsyncStorage.setItem(SEARCH_HISTORY,JSON.stringify(history));
}

export async function searchProductsApi(search:any,idcity:any){

    try {
        const url = `${API_URL}/api/searchProduct/${search}/${idcity}`;
        const response = await fetch(url);
        const result= await response.json();
        return result;
    } catch (error) {
        return error;
        
    }
}