import AsyncStorage from '@react-native-async-storage/async-storage';
import { IDUSER } from '../properties/constants';

//Setea el token
export async function setTokenApi(iduser:any){
    try {
      await AsyncStorage.setItem(IDUSER,JSON.stringify(iduser));
      return true;
    } catch (error) {
      return null;
    }
}
//Obtiene el token
export async function getTokenApi(){
    try {
      const token= await AsyncStorage.getItem(IDUSER);
      return token;
    } catch (error) {
      return null;
    }
}
//Elimina el token
export async function removeTokenApi(){
    try {
        await AsyncStorage.removeItem(IDUSER);
        return true;
      } catch (error) {
        return null;
      }
  }