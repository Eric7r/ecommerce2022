import { StackScreenProps } from "@react-navigation/stack";
import { RootStackParams } from "../navigator/StackNavigator";
import { View, StyleSheet, ToastAndroid, Platform, Text } from "react-native";
import { Button } from "react-native-paper";
import React, { useEffect, useState } from "react";
import {
  getCatMetPago,
  getCatUsoCFDI,
  solicitarFactura,
} from "../properties/services";
import * as DocumentPicker from "expo-document-picker";
import { colores } from "../theme/styles";
import Toast from "react-native-root-toast";
import SelectList from "react-native-dropdown-select-list";

interface Props extends StackScreenProps<RootStackParams, "FacturaScreen"> {}

export default function FacturaScreen({ route, navigation }: Props) {
  const [selectedFile, setSelectedFile] = useState<any>(null);
  const [form, setForm] = useState({
    IdCatMetododePago: "",
    IdCatUsoCFDI: "",
  });

  const idVenta = route?.params?.idVenta;

  //usoCFDI
  const [catCFDI, setCatCFDI] = useState<any>();
  const [jsonCFDI, setJsonCFDI] = useState([]);
  const [cfdiSelected, setCfdiSelected] = useState();

  //metodoPago
  const [catMetPago, setCatMetPago] = useState<any>();
  const [jsonPago, setJsonPago] = useState([]);
  const [pagoSelected, setPagoSelected] = useState();

  //getCatUsoCFDI
  useEffect(() => {
    (async () => {
      const response = await getCatUsoCFDI();
      const newJson = [];
      for (let i = 0; i < response.length; i++) {
        const row = response[i];
        const cfdiExists = jsonCFDI.some(
          (item) => item.key === row.IdCatUsoCFDI
        );

        if (!cfdiExists) {
          newJson.push({
            key: row.IdCatUsoCFDI,
            value: row.NomCFDI,
          });
        }
      }
      setCatCFDI(newJson);
    })();
  }, []);

  //getCatMetPago
  useEffect(() => {
    (async () => {
      const response = await getCatMetPago();
      const newJson = [];
      for (let i = 0; i < response.length; i++) {
        const row = response[i];
        const pagoExists = jsonPago.some(
          (item) => item.key === row.IdCatMetododePago
        );

        if (!pagoExists) {
          newJson.push({
            key: row.IdCatMetododePago,
            value: row.NomMetododePago,
          });
        }
      }
      setCatMetPago(newJson);
    })();
  }, []);

  const onChange = (value: string, field: string) => {
    setForm({
      ...form,
      [field]: value,
    });
  };

  const handleUpload = async () => {
    try {
      const document = await DocumentPicker.getDocumentAsync({});
      if (document.type === "success") {
        setSelectedFile(document);
      }
    } catch (error) {
      console.error("Error al seleccionar el archivo:", error);
    }
  };

  const handleSubmit = async () => {
    if (!form.IdCatUsoCFDI) {
      Platform.OS === "android"
        ? ToastAndroid.show("Seleccione el uso de CFDI", ToastAndroid.LONG)
        : Toast.show("Seleccione el uso de CFDI", {
            position: Toast.positions.CENTER,
          });
      return;
    }

    if (!form.IdCatMetododePago) {
      Platform.OS === "android"
        ? ToastAndroid.show("Seleccione el metodo de pago", ToastAndroid.LONG)
        : Toast.show("Seleccione el metodo de pago", {
            position: Toast.positions.CENTER,
          });
      return;
    }

    if (!selectedFile) {
      Platform.OS === "android"
        ? ToastAndroid.show(
            "Seleccione el comprobante fiscal",
            ToastAndroid.LONG
          )
        : Toast.show("Seleccione el comprobante fiscal", {
            position: Toast.positions.CENTER,
          });
      return;
    }

    if (selectedFile) {
      try {
        let localUri = selectedFile.uri;
        let fileName = selectedFile.name;
        let type = selectedFile.mimeType;

        let fileData = {
          uri: localUri,
          name: fileName,
          type,
        };

        const response = await solicitarFactura(
          idVenta,
          fileData,
          form.IdCatUsoCFDI,
          form.IdCatMetododePago
        );

        Platform.OS === "android"
          ? ToastAndroid.show(response.result, ToastAndroid.LONG)
          : Toast.show(response.result, { position: Toast.positions.CENTER });

        navigation.pop();
        // console.log(response.result);
      } catch (error) {
        console.error("Error uploading file:", error);
      }
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Ingrese sus datos</Text>
      <View style={styles.select}>
        <Text style={styles.label}>Uso CFDI</Text>
        <SelectList
          setSelected={setCfdiSelected}
          data={catCFDI}
          onSelect={() => onChange(cfdiSelected, "IdCatUsoCFDI")}
          placeholder="Seleccione CFDI"
        />
      </View>

      <View style={styles.select}>
        <Text style={styles.label}>Método de pago</Text>
        <SelectList
          style={styles.inputButton}
          setSelected={setPagoSelected}
          data={catMetPago}
          onSelect={() => onChange(pagoSelected, "IdCatMetododePago")}
          placeholder="Seleccione Metodo de pago"
        />
      </View>

      <View style={styles.select}>
        <Text style={styles.label}>Comprobante fiscal</Text>
        <Button
          style={styles.btnSecondary}
          icon={"file-outline"}
          mode="outlined"
          color={colores.blue}
          uppercase={false}
          onPress={handleUpload}>
          Selecciona un archivo
        </Button>
        {selectedFile && <Text>{selectedFile.name}</Text>}
      </View>

      <View style={styles.select}>
        <Button
          style={styles.btnPrimary}
          mode="contained"
          uppercase={false}
          onPress={handleSubmit}>
          <Text style={{ color: "white" }}>Guardar</Text>
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  inputMargin: {
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    height: 50,
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    color: colores.primary,
    fontWeight: "bold",
  },
  select: {
    paddingVertical: 10,
  },
  inputButton: {},
  btnPrimary: {
    height: 45,
    justifyContent: "center",
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: "#ff6900",
  },
  btnSecondary: {
    height: 45,
    justifyContent: "center",
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
  },
  label: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 4,
  },
});
