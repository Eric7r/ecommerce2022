import {
  View,
  Text,
  StyleSheet,
  ToastAndroid,
  TouchableOpacity,
  Platform,
} from "react-native";
import React, { useEffect, useState } from "react";
import { Button, TextInput } from "react-native-paper";
import { colores } from "../theme/styles";
import { ScrollView } from "react-native-gesture-handler";
import {
  getAddressByCity,
  getCitys,
  registerAddress,
} from "../properties/services";
import useAuth from "../hooks/useAuth";
import SelectList from "react-native-dropdown-select-list";
import { Loading } from "../components/Loading";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-root-toast";

interface interfaceCitys {
  IdCatCiudades: null;
  Descripcion: null;
}

export default function AddAddressScreen() {
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  const { auth } = useAuth();

  //Select de ciudad
  const [citys, setCitys] = useState<interfaceCitys>();
  const [citySelected, setCitySelected] = useState(5);
  const [jsonCity, setJsonCity] = useState([]);
  const [selectCountCity, setSelectCountCity] = useState(1);

  //Select de direcciones
  const [coloniaSelected, setColoniaSelected] = useState(1);
  const [jsonColonia, setJsonColonia] = useState([]);
  const [selectCountColonia, setSelectCountColonia] = useState(1);

  const [form, setForm] = useState({
    idciudad: "",
    codigo_postal: "",
    direccion: "",
    colonia: "",
    numero_ext: "",
    numero_int: "",
    telefono: "",
    user_id: auth?.iduser,
  });

  //Obtiene las ciudades
  useEffect(() => {
    (async () => {
      const response = await getCitys();
      for (let i = 0; i < response.length; i++) {
        if (selectCountCity == 1) {
          jsonCity.push({
            key: response[i].IdCatCiudades,
            value: response[i].Descripcion,
          });
          setSelectCountCity(selectCountCity + 1);
        }
      }
      setCitys(response);
      setLoading(false);
    })();
  }, []);

  //Formulario de registro
  const onChange = (value: string, field: string) => {
    setForm({
      ...form,
      [field]: value,
    });
  };

  //Servicio registro de direccion
  const registrarAddress = async () => {
    setLoading(true);
    try {
      const response = await registerAddress(form);
      if (response.status == 200) {
        setForm({
          idciudad: "",
          codigo_postal: "",
          direccion: "",
          colonia: "",
          numero_ext: "",
          numero_int: "",
          telefono: "",
          user_id: 0,
        });
        setLoading(false);
        navigation.goBack();
        Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); 
      } else {
        setLoading(false);
        Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); 
      }
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  //Validar formulario
  const validateForm = () => {
    let msg = "";
    if (
      form.idciudad == ""
        ? (msg = "ciudad")
        : form.codigo_postal == ""
        ? (msg = "código postal")
        : form.direccion == ""
        ? (msg = "dirección")
        : form.colonia == ""
        ? (msg = "colonia")
        : form.numero_ext == ""
        ? (msg = "número exterior")
        : form.telefono == ""
        ? (msg = "teléfono")
        : ""
    ) {
      Platform.OS === 'android'?
      ToastAndroid.show("El campo " + msg + " esta vacio", ToastAndroid.LONG):
      Toast.show("El campo " + msg + " esta vacio", { position: Toast.positions.CENTER, }); 
    } else {
      //Si no hay campos vacios hace el registro
      registrarAddress();
    }
  };
  

  return (
    <>
      {!citys ? (
        <Loading />
      ) : (
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.title}>Ingrese sus datos</Text>
            <SelectList
              setSelected={setCitySelected}
              data={jsonCity}
              onSelect={() => onChange(citySelected, "idciudad")}
              placeholder="Seleccione la ciudad"
            />
                  
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "codigo_postal")}
              label={"Codigo postal"}
            />
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "direccion")}
              label={"Dirección"}
            />
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "colonia")}
              label={"Colonia"}
            />
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "numero_ext")}
              label={"Numero exterior"}
            />
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "numero_int")}
              label={"Numero interior"}
            />
            <TextInput
              style={styles.btnInput}
              mode="outlined"
              keyboardType="number-pad"
              theme={{ colors: { primary: colores.blue }, roundness: 15 }}
              onChangeText={(value) => onChange(value, "telefono")}
              label={"Teléfono"}
            />
            <Button
              style={styles.btnAddDireccion}
              icon="map-marker"
              mode="contained"
              loading={loading}
              uppercase={false}
              onPress={() => validateForm()}
            >
              <Text style={{ color: "white" }}>Crear dirección</Text>
            </Button>
          </ScrollView>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colores.white,
    paddingBottom: 20,
    marginBottom:50
  },
  title: {
    fontSize: 20,
    paddingVertical: 10,
    textAlign: "center",
    color: colores.primary,
    fontWeight: "bold",
  },
  btnInput: {
    marginVertical: 5,
    backgroundColor: "white",
  },
  btnAddDireccion: {
    marginTop: 10,
    marginBottom:20,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: colores.blue,
  },
});
