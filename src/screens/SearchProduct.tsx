import React, { useEffect, useState } from "react";
import { Text } from "react-native";
import { View } from "react-native";
import { searchProductsApi } from "../api/search";
import { Search } from "../components/Search";
import { StatusBarCustom } from "../components/StatusBar";
import { colores } from "../theme/styles";
import { size } from 'lodash';
import { Loading } from "../components/Loading";
import { StyleSheet } from "react-native";
import ProductList from "../components/ProductList";
import useAuth from "../hooks/useAuth";

export const SearchProduct = (props:any) => {

    const {route}= props;
    const {params}= route;
    const [products, setProducts] = useState(null);
    const {auth} = useAuth();

    useEffect(() => {
      (async()=>{
        setProducts(null);
        const response= await searchProductsApi(params.search,auth?.idcity);
        setProducts(response);
        // console.log("busqueda:"+products);
        
      })()
    }, [params.search])
    

  return (
    <>
      <StatusBarCustom backgroundColor={colores.black} barStyle="light-content"/>
      <Search currentSearch={params.search}/>
    {!products ? (
      <Loading/>
    ) : size(products)===0 ?(
      <View style={styles.container}>
        <Text style={styles.searchText}>No existen articulos en stock para{" "} 
          <Text style={{color:colores.primary}}>'{params.search}'</Text>
        </Text>
        <Text style={styles.otherText}>Revisa la ortografía o usa términos mas generales.</Text>
      </View>
    ) : (
      <ProductList products={products}/>
    )}
    </>
    
  );
};

const styles = StyleSheet.create({
  container:{
    padding:15
  },
  searchText:{
    fontSize:16,
    fontWeight:'bold'
  },
  otherText:{
    fontSize:14,
    paddingTop:5
  }
})
