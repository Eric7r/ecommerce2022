import React, { useState } from "react";
import { KeyboardAvoidingView, Platform, StyleSheet, View } from "react-native";
import { LoginScreen } from "./LoginScreen";
import RecoveryScreen from "./RecoveryScreen";
import { RegisterScreen } from "./RegisterScreen";

export const Auth = (props: any) => {
  const { startScreen } = props;
  const [showLogin, setshowLogin] = useState(true);
  const changeForm = () => setshowLogin(!showLogin);
  console.log("aut: " + startScreen);

  return (
    <View>
      {Platform.OS === "ios" ? (
        <KeyboardAvoidingView behavior={"padding"} style={styles.marginScreen}>
          {startScreen == "loginScreen" && (
            <LoginScreen changeForm={changeForm} />
          )}
          {startScreen == "recoveryScreen" && <RecoveryScreen />}
          {startScreen == "registerScreen" && (
            <RegisterScreen changeForm={changeForm} />
          )}
        </KeyboardAvoidingView>
      ) : (
        <KeyboardAvoidingView style={styles.marginScreen}>
          {startScreen == "loginScreen" && (
            <LoginScreen changeForm={changeForm} />
          )}
          {startScreen == "recoveryScreen" && <RecoveryScreen />}
          {startScreen == "registerScreen" && (
            <RegisterScreen changeForm={changeForm} />
          )}
        </KeyboardAvoidingView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  marginScreen: {
    marginBottom: 20,
  },
});
