
import React, { useEffect, useState } from "react";
import {
  Modal,
  Platform,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { CheckBox, Header } from "react-native-elements";
import { Button, TextInput } from "react-native-paper";
import { getCitys, registerUser } from "../properties/services";
import { colores } from "../theme/styles";
import SelectList from "react-native-dropdown-select-list";
import Toast from "react-native-root-toast";
import useAuth from "../hooks/useAuth";
import Icon from "react-native-vector-icons/FontAwesome";
import {size} from "lodash";


interface interfaceCitys {
  IdCatCiudades: null;
  Descripcion: null;
}

export const RegisterScreen = (props: any) => {
  const { changeForm } = props;
  const [loading, setLoading] = useState(false);
  const [catCitys, setCatCitys] = useState([]);
  const [privacidad, setPrivacidad] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);

  //Select de ciudad
  const [citys, setCitys] = useState<interfaceCitys>();
  const [citySelected, setCitySelected] = useState(5);
  const [jsonCity, setJsonCity] = useState([]);
  const [selectCountCity, setSelectCountCity] = useState(1);
  const { recovery }=useAuth();

  const [form, setForm] = useState({
    nombre: "",
    username: "",
    password: "",
    confirm_password: "",
    direccion: "",
    telefono: "",
    ciudad: "",
    email: "",
    codigo_postal: "",
    colonia: "",
  });

  //Obtiene las ciudades
  useEffect(() => {
    (async () => {
      const response = await getCitys();
      for (let i = 0; i < response.length; i++) {
        if (selectCountCity == 1) {
          jsonCity.push({
            key: response[i].IdCatCiudades,
            value: response[i].Descripcion,
          });
          setSelectCountCity(selectCountCity + 1);
        }
      }
      setCitys(response);
      setLoading(false);
    })();
  }, []);

  //Formulario de registro
  const onChange = (value: string, field: string) => {
    setForm({
      ...form,
      [field]: value,
    });
  };
  //console.log(form);

  const setCheck= ()=>{
    if(privacidad){
      setPrivacidad(false)
      setModalVisible(false)
    }else{
      setPrivacidad(true)
      setModalVisible(true)
    }
  }

  //Servicio registro de usuario
  const registrarUsuario = async () => {
    setLoading(true);
    console.log(form);
    
    try {
      const response = await registerUser(form);
      if (response.status == 200) {
        setForm({
          nombre: "",
          username: "",
          password: "",
          confirm_password:"",
          direccion: "",
          telefono: "",
          ciudad: "",
          email: "",
          codigo_postal: "",
          colonia: "",
        });
        setLoading(false);
        Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); 
        changeForm();
        funtionRecovery("loginScreen");
      } else {
        setLoading(false);
        Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); 
      }
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  //Valida el correo
  const validateEmail=(email: string): boolean => {
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    return emailRegex.test(email);
  }

  //Validar formulario
  const validateForm = () => {
    let msg = "";
    //Input genericos
    if ( 
      size(form.nombre)<3
        ? (msg = "nombre")
        : size(form.username)<3
        ? (msg = "usuario")
        : size(form.password)<3
        ? (msg = "contraseña")
        : size(form.direccion)<3
        ? (msg = "dirección")
        : size(form.colonia)<3
        ? (msg = "colonia")
        : ""
    ) {
      Platform.OS === 'android'?
      ToastAndroid.show("El campo " + msg + " debe tener al menos 3 caracteres", ToastAndroid.LONG):
      Toast.show("El campo " + msg + " debe tener al menos 3 caracteres", { position: Toast.positions.CENTER, });
    //Matchea la password  
    }else if(form.password !== form.confirm_password){
      Platform.OS === 'android'?
      ToastAndroid.show("Las contraseñas no coinciden", ToastAndroid.LONG):
      Toast.show("Las contraseñas no coinciden", { position: Toast.positions.CENTER, }); 
    }
     //Ciudad
     else if(form.ciudad == ""){
      Platform.OS === 'android'?
      ToastAndroid.show("La ciudad no debe estar vacia", ToastAndroid.LONG):
      Toast.show("La ciudad no debe estar vacia", { position: Toast.positions.CENTER, }); 
    }
    //Codigo postal
    else if(size(form.codigo_postal)!==5){
      Platform.OS === 'android'?
      ToastAndroid.show("Cantidad de carácteres no valido en el código postal", ToastAndroid.LONG):
      Toast.show("Cantidad de carácteres no valido en el código postal", { position: Toast.positions.CENTER, }); 
    }
    //input email
    else if(!validateEmail(form.email)){
      Platform.OS === 'android'?
      ToastAndroid.show("Correo no válido", ToastAndroid.LONG):
      Toast.show("Correo no válido", { position: Toast.positions.CENTER, }); 
    }
    //Valida numero telefono
    else if(size(form.telefono)!==10){
      Platform.OS === 'android'?
      ToastAndroid.show("El número de teléfono debe ser de 10 digitos", ToastAndroid.LONG):
      Toast.show("El número de teléfono debe ser de 10 digitos", { position: Toast.positions.CENTER, }); 
    }
    else {
      //Si se cumplen las validaciones de inputs hace el registro
      registrarUsuario();
    }
  };

  //Get a catalogos de ciudades
  useEffect(() => {
    (async () => {
      const response = await getCitys();
      setCatCitys(response);
    })();
  }, []);

  const funtionRecovery=(value:any)=>{
    recovery(value);
  }

  return (
      <ScrollView showsVerticalScrollIndicator={false} >
        <View style={styles.container}>
        {(Platform.OS === 'android') &&
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={()=>funtionRecovery("loginScreen")} style={{marginLeft:10}}>
              <Text style={styles.backButtonText}><Icon name="arrow-left" size={30} color="white" /></Text>
            </TouchableOpacity>
            <Text style={styles.headerTitle}>Registro</Text>
          </View>
        }

        {(Platform.OS === 'ios') &&
          <Header
          backgroundColor={colores.primary}
          leftComponent={
            <TextInput.Icon
              style={{ marginTop:120, marginLeft: 30 }}
              name="arrow-left-circle"
              color="white"
              size={40}
              onPress={()=>funtionRecovery("loginScreen")}
            />
          }
          centerComponent={{ text: "Registro", style: styles.heading }}/>
        }

          <Text style={{ fontSize: 18, alignSelf: "center", marginTop: 10, color:colores.black, fontWeight:'bold' }}>
            Registra tus datos
          </Text>
          <TextInput
            onChangeText={(value) => onChange(value, "nombre")}
            style={styles.inputMargin}
            mode="outlined"
            label="Nombre"
            placeholder="Ingrese nombre"
            left={
              <TextInput.Icon name="account-circle" color={colores.primary} />
            }
          />
          <TextInput
            onChangeText={(value) => onChange(value, "username")}
            style={styles.inputMargin}
            mode="outlined"
            label="Usuario"
            placeholder="Ingrese usuario"
            left={<TextInput.Icon name="fingerprint" color={colores.primary} />}
          />
          <TextInput
            onChangeText={(value) => onChange(value, "password")}
            style={styles.inputMargin}
            mode="outlined"
            label="Contraseña"
            placeholder="Ingrese contraseña"
            left={<TextInput.Icon name="lock" color={colores.primary} />}
            secureTextEntry
          />
          <TextInput
            onChangeText={(value) => onChange(value, "confirm_password")}
            style={styles.inputMargin}
            mode="outlined"
            label="Confirmar contraseña"
            placeholder="Confirme contraseña"
            left={<TextInput.Icon name="lock" color={colores.primary} />}
            secureTextEntry
          />


          <View
            style={{
              width: "80%",
              alignSelf: "center",
              marginTop: 8,
            }}
          >
            <SelectList
              setSelected={setCitySelected}
              data={jsonCity}
              onSelect={() => onChange(citySelected, "ciudad")}
              placeholder="Seleccione la ciudad"
            />
          </View>

          <TextInput
            onChangeText={(value) => onChange(value, "direccion")}
            style={styles.inputMargin}
            mode="outlined"
            label="Direccion"
            placeholder="Ingrese direccion"
            left={<TextInput.Icon name="home" color={colores.primary} />}
          />

          <TextInput
            onChangeText={(value) => onChange(value, "colonia")}
            style={styles.inputMargin}
            mode="outlined"
            label="Colonia"
            placeholder="Ingrese colonia"
            left={<TextInput.Icon name="city-variant-outline" color={colores.primary} />}
          />

          <TextInput
            onChangeText={(value) => onChange(value, "codigo_postal")}
            style={styles.inputMargin}
            mode="outlined"
            keyboardType="numeric"
            label="Código postal"
            placeholder="Ingrese código postal"
            left={<TextInput.Icon name="map-marker" color={colores.primary} />}
          />

          <TextInput
            onChangeText={(value) => onChange(value, "email")}
            style={styles.inputMargin}
            mode="outlined"
            label="Correo"
            placeholder="Ingrese correo"
            left={<TextInput.Icon name="email" color={colores.primary} />}
          />

          <TextInput
            onChangeText={(value) => onChange(value, "telefono")}
            style={styles.inputMargin}
            keyboardType="numeric"
            mode="outlined"
            label="Telefono"
            placeholder="Ingrese telefono"
            left={<TextInput.Icon name="phone" color={colores.primary} />}
          />

          <CheckBox
            checkedIcon="check-box"
            iconType="material"
            uncheckedIcon="check-box-outline-blank"
            title="Estoy de acuerdo con la Politica de Privacidad"
            checkedTitle="He leído y estoy de acuerdo con la Politica de Privacidad"
            checked={privacidad}
            onPress={() =>
              setCheck()
            }
          />

        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {setPrivacidad(!modalVisible);}}>
            <View style={styles.centeredView}>
              <ScrollView>
              <View style={styles.modalView}>
                {/*Terminos y condiciones*/}
                <Text>Términos y Condiciones</Text>
                <Text style={styles.textAcuerdo}>Licencia y acceso </Text>
                <Text>Sujeto al cumplimiento por parte de usted de estas Condiciones de Uso y las Condiciones Generales de los Servicios aplicables así como al pago del precio aplicable, en su caso, Kowi, le conceden una licencia limitada no exclusiva, no transferible y no sub-licenciable, de acceso y utilización, a los Productos de Kowi para fines personales no comerciales; a descargar o copiar información de cuenta alguna para el beneficio de otra empresa; ni el uso de herramientas o robots de búsqueda y extracción de datos o similar. </Text>
                <Text style={styles.textAcuerdo}>Su cuenta </Text>
                <Text>Es posible que usted requiera crear una cuenta de Kowi propia, acceder a la misma y contar con un método de pago válido asociado a su cuenta para pedir los Productos de Kowi. En caso de que surja un problema con su método de pago seleccionado, podríamos realizar el cargo a cualquier otro método de pago válido asociado a su cuenta. Cuando usted utiliza los Servicios de Kowi Store es usted responsable de mantener la confidencialidad de los datos de su cuenta y su contraseña, así como de restringir el acceso a su computadora y demás dispositivos, y usted asume la responsabilidad de cualesquier actividades realizadas desde su cuenta o utilizando su contraseña. </Text>
                <Text style={styles.textAcuerdo}>Pasarela de pago: </Text>
                <Text>OPENPAY </Text>
                <Text style={styles.textAcuerdo}>Politicas de Privacidad </Text>
                <Text style={styles.textAcuerdo}>Su compra </Text>
                <Text>Una vez realizado el pago, el tiempo de entrega dependerá del servicio que se encuentre disponible para la entrega a domicilio, por lo cual también nos reservamos el derecho de hacer valido cualquier tipo de devolución o cancelación de cualquier pedido. </Text>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => setModalVisible(false)}>
                  <Text style={styles.textStyle}>Salir</Text>
                </Pressable>
              </View>
              </ScrollView>
            </View>
          </Modal>
        </View>

        <Button
          style={styles.btnRegister}
          icon="import"
          mode="contained"
          uppercase={false}
          loading={loading}
          disabled={!privacidad}
          onPress={() => validateForm()}>
          <Text style={{ color: "white" }}>Registrar</Text>
        </Button>
        </View>
      </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    marginVertical:0
  },
  backButton: {
    position: "absolute",
    zIndex: 999,
    elevation: 9,
    top: 30,
    left: 5,
  },
  inputLogin: {
    marginTop: 3,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    height: 50,
  },
  heading: {
    color: "white",
    fontSize: 25,
    fontWeight: "bold",
    marginTop: Platform.OS === 'android'?30:60,
    marginBottom:10
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:colores.primary
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize:18

  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  button: {
    borderRadius: 20,
    padding: 10,
    marginTop:20,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: colores.blue,
  },
  textAcuerdo: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
    fontSize:15
  },
  headerIOS:{
    backgroundColor:colores.primary
  },
  paddingScroll:{
    marginTop:5
  }, 
  inputMargin:{
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    height: 50,
  },
  btnRegister:{
    marginTop: 10,
    marginBottom:20,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: "#ff6900",
  },
  btnSesion:{
    marginTop: 5,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    borderRadius: 15,
    borderWidth: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colores.primary,
    height: 70,
    paddingHorizontal: 16,
    elevation: 2
  },
  backButtonText: {
    color: '#000',
    fontSize: 16,
  },
  headerTitle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color:colores.white
  },
});
