
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { Dimensions, Image, Platform, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from "react-native";
import { Button, TextInput, Title } from "react-native-paper";
import Toast from "react-native-root-toast";
import useAuth from "../hooks/useAuth";
import { loginUser } from "../properties/services";
import styles, { colores } from "../theme/styles";


export const LoginScreen = (props: any) => {
  const { changeForm } = props;
  const [loading, setLoading] = useState(false);
  const [loadingFake, setLoadingFake] = useState(false);
  const screenHeight = Dimensions.get("window").height;
  const [form,setForm]=useState({username:'',password:''})
  const { login,recovery }=useAuth();


  
  //Formulario de sesion
  const onChange=( value:string, field:string )=>{
    setForm({
      ...form,
      [field]:value
    })
  }

  const funtionRecovery=(value:any)=>{
    recovery(value);
  }


  //Servicio inicio de sesion
  const iniciarSesion= async ()=>{
    setLoading(true);
      try {
        const response = await loginUser(form);
        if(response.status==200){
          setLoading(false);
          //console.log(response);
          login(response);
          
        }
        else{
          setLoading(false);
          Platform.OS === 'android'?
          ToastAndroid.show(response.result, ToastAndroid.SHORT):
          Toast.show(response.result, { position: Toast.positions.CENTER, }); 
        }
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
  }

  const iniciarSesionGenerica= async ()=>{
    setLoadingFake(true);
      try {
        const response = await loginUser({username:'kowi',password:'123456'});
        if(response.status==200){
          setLoadingFake(false);
          //console.log(response);
          login(response);
          
        }
        else{
          setLoadingFake(false);
          Platform.OS === 'android'?
          ToastAndroid.show(response.result, ToastAndroid.SHORT):
          Toast.show(response.result, { position: Toast.positions.CENTER, }); 
        }
      } catch (error) {
        setLoadingFake(false);
        console.log(error);
      }
  }

  //Validar formulario
  const validarForm=()=>{
    let isValid=true;
    if(form.username==""){
      isValid=false;
      Platform.OS === 'android'?
      ToastAndroid.show("El campo usuario esta vacio", ToastAndroid.LONG):
      Toast.show("El campo usuario esta vacio", { position: Toast.positions.CENTER, }); 
    }
    else if(form.password==""){
      isValid=false;
      Platform.OS === 'android'?
      ToastAndroid.show("El campo contraseña esta vacio", ToastAndroid.LONG):
      Toast.show("El campo contraseña esta vacio", { position: Toast.positions.CENTER, }); 
    }
    if(isValid){
      iniciarSesion();
    }
  }

  return (
    <View >
      
      <StatusBar style="light" />
       {/**Bloque 1 Imagen */}
       <Image 
          source={require("../../assets/ecomerceLogo.png")}
          style={stylesBase.imageBackground}
        />
        
        <View style={stylesBase.form}>
          <Text style={stylesBase.formText}>Ingresa tus datos</Text>

            <View >
            {/*Input usuario */}
            
            <TextInput
              style={styles.inputLogin}
              mode="outlined"
              label="Usuario"
              onChangeText={(value)=>onChange(value,'username')}
              //placeholder="Ingrese usuario"
              theme={{ colors: { primary: colores.primary }, roundness: 20}}
              left={<TextInput.Icon name="account-circle" color={colores.primary} />}/>
          </View>

          <View >
           {/*Input contraseña */}
            <TextInput
              style={styles.inputLogin}
              mode="outlined"
              label="Contraseña"
              onChangeText={(value)=>onChange(value,'password')}
              theme={{ colors: { primary: colores.primary }, roundness: 20 }}
              placeholder="Ingrese contraseña"
              left={<TextInput.Icon name="lock" color={colores.primary} />}
              secureTextEntry/>
          </View>      

          <View>
            {/*Boton iniciar sesion*/}
            <Button
              style={styles.btnIniciarSesion}
              icon="import"
              mode="contained"
              uppercase={false}
              loading={loading}
              onPress={()=>validarForm()}>
              <Text style={{ color: "white" }}>Iniciar sesión</Text>
            </Button>
            
            {/*Boton registro*/}
            <Button
              style={styles.btnRegistrarse}
              icon={"file-account-outline"}
              mode="outlined"
              color={colores.primary}
              uppercase={false}
              onPress={()=>funtionRecovery("registerScreen")}
            >
              Registrarse
            </Button>
            {/*Boton no Login*/}
            <Button
              style={styles.btnNoLogin}
              icon={"home"}
              mode="outlined"
              color={colores.white}
              uppercase={false}
              loading={loadingFake}
              onPress={()=>iniciarSesionGenerica()}
            >
              Iniciar sesion sin cuenta
            </Button>
          </View>

          <View style={stylesBase.formRecovery}>
          <Text>No recuerdas tu contraseña?</Text>
          <TouchableOpacity onPress={ ()=>funtionRecovery("recoveryScreen")}>
            <Text style={stylesBase.formRecoveryText}>Recupérala aquí</Text>
          </TouchableOpacity>
        </View>
        
        <View style={stylesBase.textVersion}>
          <Text style={stylesBase.version}>Version 1.0.14</Text>
        </View>

        </View>
        
    </View>   
    
  );
};

const stylesBase = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  imageBackground:{
    width:'100%',
    height:'85%'
  },
  form:{
    width:'100%',
    height:'25%',
    backgroundColor:'white',
    position:'absolute',
    bottom:0,
    borderTopLeftRadius:40,
    borderTopRightRadius:40,
    padding:10
  },
  formText:{
    fontWeight:'bold',
    alignSelf:'center',
    fontSize:16,
    marginTop:10
    
  },
  formIcon:{
    width:25,
    height:25,
    marginTop:5
  },
  formInput:{
    flexDirection:'row',
    marginTop:30,
    
  },
  formTextInput:{
    flex:1,
    borderBottomWidth:1,
    borderBottomColor:'#AAAAAA',
    marginLeft:15
  },
  formRegister:{
    flexDirection:'row',
    justifyContent:'center',
    marginTop:30
  },
  formRegisterText:{
    fontStyle: 'italic',
    color:'orange',
    borderBottomWidth:1,
    borderBottomColor:'orange',
    fontWeight:'bold',
    marginLeft:10
  },
  logoContainer:{
    position:'absolute',
    alignSelf:'center',
    top:'15%'
  },
  logoImage:{
    width:100,
    height:100
  },
  logoText:{
    color:'white',
    fontSize:20,
    textAlign:'center',
    marginTop:10,
    fontWeight:'bold'
  },
  formRecovery:{
    flexDirection:'row',
    justifyContent:'center',
    marginTop:15
  },
  formRecoveryText:{
    fontStyle: 'italic',
    color:colores.primary,
    borderBottomWidth:1,
    borderBottomColor:colores.primary,
    fontWeight:'bold',
    marginLeft:10
  },
  textVersion:{
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  version:{
    fontSize:11,
    fontWeight:'bold',
    marginTop:8
  }
  
});
