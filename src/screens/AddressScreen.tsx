import { View, Text, StyleSheet } from "react-native";
import React, { useCallback, useState } from "react";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { IconButton } from "react-native-paper";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { getAddressApi } from "../api/address";
import useAuth from "../hooks/useAuth";
import AddressList from "../components/AddressList";

export default function AddressScreen() {
  const [address, setAddress] = useState(null);
  const [reloadAddress, setReloadAddress] = useState(false);
  const { auth } = useAuth();
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      setAddress(null);
      (async () => {
        const response = await getAddressApi(auth);
        setAddress(response);
        setReloadAddress(false);
      })();
    }, [reloadAddress])
  );

  return (
    <ScrollView style={styles.container}>
      <View style={{marginBottom:100}}>
        <Text style={styles.title}>Mis direcciones</Text>

        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("addaddress" as never)}
        >
          <View style={styles.addAdress}>
            <Text style={styles.addAdressText}>Añadir una direccion</Text>
            <IconButton icon="arrow-right" color="#000" size={19} />
          </View>
        </TouchableWithoutFeedback>

        <Text style={styles.subtitle}>Lista de direcciones</Text>
        <AddressList addresses={address} setReloadAddress={setReloadAddress} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: "white",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  addAdress: {
    borderWidth: 0.9,
    borderRadius: 5,
    borderColor: "#ddd",
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  addAdressText: {
    fontSize: 16,
  },
  subtitle: {
    fontSize: 14,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 5,
  },
});
