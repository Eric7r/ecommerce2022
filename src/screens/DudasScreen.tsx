import { View, Text, StyleSheet } from 'react-native'
import React, { useEffect, useState }  from 'react'
import { colores } from '../theme/styles';
import useAuth from '../hooks/useAuth';
import { getAclaraciones } from '../properties/services';

export default function DudasScreen() {
  
  interface catAclaracion{
    "IdCatAclaraciones": 0,
    "Nombre": '',
    "Direccion": '',
    "Correo": '',
    "Telefono": '',
    "IdCatCiudad": ''
  };

  const { auth } = useAuth();
  const [info, setInfo] = useState<catAclaracion>();
  

  useEffect(() => {
    (async () => {
      const response = await getAclaraciones(auth?.idcity);
      setInfo(response);
      console.log(info);
    })();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Atención:</Text>
        <Text style={styles.subTitle}>{info?.Nombre}</Text>
      <Text style={styles.title}>Dirección:</Text>
        <Text style={styles.subTitle}>{info?.Direccion}</Text>
      <Text style={styles.title}>Teléfono:</Text>
        <Text style={styles.subTitle}>Tel. {info?.Telefono}</Text>
      <Text style={styles.title}>Correo:</Text>
        <Text style={styles.subTitle}>{info?.Correo}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    padding: 30,
    backgroundColor: "white",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    color:colores.primary,
    marginTop:5
  },
  subTitle: {
    fontSize: 16,
  }
});
