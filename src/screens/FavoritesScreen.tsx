import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Search } from "../components/Search";
import { StatusBarCustom } from "../components/StatusBar";
import { colores } from "../theme/styles";

export const FavoritesScreen = () => {
  return (
    <>
     <StatusBarCustom backgroundColor={colores.black} />
      <Search/>
      <View style={styles.container}>
      {/*<StatusBar/>*/}
      <Text>FavoritesScreen</Text>
    </View>
    </>
    
  );
};

const styles=StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
})
