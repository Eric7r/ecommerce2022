import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
} from "react-native";
import React, { useEffect, useState } from "react";
import useAuth from "../hooks/useAuth";
import { getOrdersByUser } from "../properties/services";
import { Loading } from "../components/Loading";
import { map } from "lodash";
import { API_URL_IMAGES } from "../properties/constants";
import OrderHistory from "../components/OrderHistory";
import { colores } from "../theme/styles";
import { StatusBar } from "expo-status-bar";

export default function OrderScreen() {
  const { auth } = useAuth();
  const [pedidos, setPedidos] = useState(null);

  useEffect(() => {
    (async () => {
      const response = await getOrdersByUser(auth?.iduser);
      setPedidos(response);
    })();
  }, []);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.container}>
        {!pedidos ? (
          <View style={{ padding: 32 }}>
            <Loading />
          </View>
        ) : pedidos?.length == 0 ? (
          <Text style={styles.title}>No has hecho ningun pedido todavía</Text>
        ) : (
          map(pedidos, (pedidos: any, index: any) => (
            <OrderHistory key={index} pedidos={pedidos} />
          ))
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colores.white,
    paddingBottom: Dimensions.get("window").height * 0.15,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    alignSelf: "center",
    color: colores.black,
    marginTop: 15,
  },
});
