import { useFocusEffect } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { getProductCartApi } from "../api/cart";
import { Search } from "../components/Search";
import { StatusBarCustom } from "../components/StatusBar";
import { colores } from "../theme/styles";
import { size } from "lodash";
import NotProducts from "../components/NotProducts";
import { ScrollView } from "react-native-gesture-handler";
import ProductListCarrito from "../components/ProductListCarrito";
import { getAddressApi } from "../api/address";
import useAuth from "../hooks/useAuth";
import AddressCart from "../components/AddressCart";
import Payment from "../components/Payment";
import { getCentroVenta, getCitys } from "../properties/services";
import SelectList from 'react-native-dropdown-select-list';
import { TextInput } from "react-native-paper";

interface interfaceCitys {
  IdCatCiudades: null;
  Descripcion: null;
}

export const CartScreen = () => {
  const [cart, setCart] = useState(null);
  const [products, setProducts] = useState(null);
  const [reloadCart, setReloadCart] = useState(false);
  const [addresses, setAddresses] = useState(null);
  const [selectedAddress, setSelectedAddress] = useState(null);
  const [totalPayment, setTotalPayment] = useState(null);
  const {auth}= useAuth();
  const [comentario, setComentario] =useState("");

  //Select de ciudad
  const [citys, setCitys] = useState<interfaceCitys>();
  const [citySelected, setCitySelected] = useState(null);
  const [jsonCity, setJsonCity] = useState([]);
  const [selectCountCity, setSelectCountCity] = useState(1);
  const [costoEnvio, setCostoEnvio] = useState(0);
  
  useFocusEffect(
    useCallback(() => {
      setCart(null);
      setAddresses(null);
      setCitySelected(null);
      setComentario("");
     
      loadCart();
      loadAdresses();
    }, [])
  );

  useEffect(() => {
    if(reloadCart){
      loadCart();
      setReloadCart(false);
    }
  }, [reloadCart]);

  //Get de los productos del carrito
  const loadCart = async () => {
    const response = await getProductCartApi(auth?.iduser);
    setCart(response);
  };

  //Get de las direcciones de envio del usuario
  const loadAdresses = async () => {
    const response = await getAddressApi(auth); 
    setAddresses(response); 
  };

   //Obtiene las ciudades
   useEffect(() => {
    (async () => {
      const response = await getCitys();
      for (let i = 0; i < response.length; i++) {
        if (selectCountCity == 1) {
          jsonCity.push({
            key: response[i].IdCatCiudades,
            value: response[i].Descripcion,
          });
          setSelectCountCity(selectCountCity + 1);
        }
      }
      setCitys(response);
    })();
  }, []);

  //Cambia el costo de envio dependiendo de la ciudad
  useEffect(() => {
    (async () => {
      if(citySelected==null){
        setCostoEnvio(0);
        const response = await getCentroVenta(auth?.idcity);
        setCostoEnvio(response.CostoEnvio);
      }else{
        setCostoEnvio(0);
        const response = await getCentroVenta(citySelected);
        setCostoEnvio(response.CostoEnvio);
        //console.log(costoEnvio);
      }
    })();
  }, [citySelected]);

//Si cambia el valor del envio en el back este lo refresca
  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      const fetchUser = async () => {
        try {
          if (isActive) {
            const response = await getCentroVenta(auth?.idcity);;
            setCostoEnvio(response.CostoEnvio);
          }
        } catch (e) {
          console.log(e);
        }
      };
      fetchUser();
      return () => {
        isActive = false;
      };
    }, [])
  );

  return (
    <>
      <StatusBarCustom
        backgroundColor={colores.black}
        //barStyle="light-content"
      />
      <Search />
      {!cart && size(cart) === 0 ? (
        <NotProducts />
      ) : (
        <ScrollView style={styles.cartContainer}>
           {size(cart) !== 0  && (
           <View style={styles.rowCity}>
              <Text style={styles.titleCity}>Cambiar ciudad:</Text>
              <SelectList
                  setSelected={setCitySelected}
                  data={jsonCity}
                  //onSelect={() => onChange(citySelected, "ciudad")}
                  placeholder="Seleccione la ciudad"
                />
            </View>
            )}
           
          <ProductListCarrito
            cart={cart}
            products={products}
            setProducts={setProducts}
            setReloadCart={setReloadCart}
            setTotalPayment={setTotalPayment}
            citys={citys}
          />

          {size(cart) !== 0 && (<Text style={styles.costo}>Costo de envio:<Text style={styles.text}>${costoEnvio} MXN</Text></Text>)}

          {size(cart) !== 0  && (<AddressCart addresses={addresses} selectedAddress={selectedAddress} products={products} setSelectedAddress={setSelectedAddress}/>)}
          {size(cart) !== 0  && (<TextInput label="Comentario del pedido" value={comentario} onChangeText={text => setComentario(text)} 
          mode="outlined"
          multiline={true}
          numberOfLines={10}
          style={{ height:150, textAlignVertical: 'top',}}
          theme={{ colors: { primary: colores.blue }, roundness: 15 }}/>)}
          {size(cart) !== 0  && (<Payment products={products} selectedAddress={selectedAddress} totalPayment={totalPayment} citySelected={citySelected} comentario={comentario} costoEnvio={costoEnvio} setCostoEnvio={setCostoEnvio}/> )}
          
        </ScrollView>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  cartContainer: {
    padding: 10,
    backgroundColor:"white",
    marginBottom:50
  },
  rowCity:{
    flexDirection:'row',
    alignSelf:'center'
  },
  titleCity: {
    fontSize: 15,
    marginTop:10,
    marginRight:10,
    fontWeight: "bold",
    textAlign:'center'
  },
  input:{
    height:100
  },
  costo:{
    fontSize: 15,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  text:{
    color:colores.blue
  }
});
