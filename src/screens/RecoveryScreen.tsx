import { View, Text, StyleSheet, SafeAreaView, ScrollView, Platform, ToastAndroid } from 'react-native'
import React, { useState } from 'react'
import { Button, TextInput } from 'react-native-paper'
import useAuth from '../hooks/useAuth';
import { StatusBar } from 'expo-status-bar';
import { colores } from '../theme/styles';
import { Header } from 'react-native-elements';
import Toast from 'react-native-root-toast';
import { restorePassowrd } from '../properties/services';

export default function RecoveryScreen() {

  const { recovery }=useAuth();
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    correo: ""
  });

  const funtionRecovery=(value:any)=>{
    recovery(value);
  }
  
  const onChange = (value: string, field: string) => {
      setForm({
        ...form,
        [field]: value,
      });
      //console.log(form);
      
    };

    const validateForm = () => {
      let msg = "";
      if (
        form.correo == ""
          ? (msg = "correo"): ""
      ) {
        Platform.OS === 'android'?
        ToastAndroid.show("El campo " + msg + " esta vacio", ToastAndroid.LONG):
        Toast.show("El campo " + msg + " esta vacio", { position: Toast.positions.CENTER, }); 
      } else {
        //Si no hay campos vacios hace el update
        restorePass();
      }
    };
  
    const restorePass = async () => {
      setLoading(true);
      
      try {
        const response = await restorePassowrd(form);
        if (response.status == 200) {
          setForm({
            correo: ""
          });
          setLoading(false);
          Platform.OS === 'android'?
          ToastAndroid.show(response.result, ToastAndroid.LONG):
          Toast.show(response.result, { position: Toast.positions.CENTER, });
          funtionRecovery("loginScreen");
        } else {
          setLoading(false);
          Platform.OS === 'android'?
          ToastAndroid.show(response.result, ToastAndroid.LONG):
          Toast.show(response.result, { position: Toast.positions.CENTER, }); 
        }
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
    };

  return (
    <ScrollView showsVerticalScrollIndicator={false} >
      <View style={styles.container}>
        <Header
          backgroundColor={colores.primary}
          leftComponent={
            <TextInput.Icon
              style={{ marginTop: Platform.OS === 'android'?70:120, marginLeft: 30 }}
              name="arrow-left-circle"
              color="white"
              size={40}
              onPress={()=>funtionRecovery("loginScreen")}
            />
          }
          centerComponent={{ text: "Recuperar contraseña", style: styles.heading }}
          
        />

        <Text style={{ fontSize: 16, alignSelf: "center", marginTop: 20 }}>
          Ingresa tu correo
        </Text>
        <TextInput
            onChangeText={(value) => onChange(value, "correo")}
            style={styles.inputMargin}
            mode="outlined"
            label="Correo"
            placeholder="Ingrese correo"
            left={<TextInput.Icon name="mail" color={colores.primary} />}
          />
        
        <Text style={{textAlign:'center',fontWeight:'bold', marginTop:10, color:'#267ED6'}}>Se le enviará un correo con su nueva contraseña</Text>

        <Button
          style={styles.btnRegister}
          icon="import"
          mode="contained"
          uppercase={false}
          loading={loading}
          onPress={()=>validateForm()}
        >
          <Text style={{ color: "white" }}>Aceptar</Text>
        </Button>
        <Button
          style={styles.btnSesion}
          icon={"file-account-outline"}
          mode="outlined"
          color="#ff6900"
          uppercase={false}
          onPress={()=>funtionRecovery("loginScreen")}
        >
          Iniciar sesion
        </Button>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    marginVertical:0
  },
  backButton: {
    position: "absolute",
    zIndex: 999,
    elevation: 9,
    top: 30,
    left: 5,
  },
  inputLogin: {
    marginTop: 3,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    height: 50,
  },
  heading: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginTop: Platform.OS === 'android'?30:60,
    marginBottom:10
   
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:colores.primary
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize:18

  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  button: {
    borderRadius: 20,
    padding: 10,
    marginTop:20,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: colores.blue,
  },
  textAcuerdo: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
    fontSize:15
  },
  headerIOS:{
    backgroundColor:colores.primary,
    //marginTop:20
  },
  paddingScroll:{
    marginTop:5
  }, 
  inputMargin:{
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    height: 50,
  },
  btnRegister:{
    marginTop: 10,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: "#ff6900",
  },
  btnSesion:{
    marginTop: 5,
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
    borderRadius: 15,
    borderWidth: 1,
  }
});