import { ActivityIndicator, RefreshControl, ScrollView, StyleSheet, Text, View } from "react-native";
import { HorizontalSlider } from "../components/HorizontalSlider";
import { useProducts } from "../hooks/useProducts";
import { StatusBarCustom } from "../components/StatusBar";
import { colores } from "../theme/styles";
import { Search } from "../components/Search";
import { size } from "lodash";
import React, { useState } from "react";
import NotProducts from "../components/NotProducts";

export const HomeScreen = () => {


  var {
    isLoading,
    productosCategory1,
    productosCategory2,
    productosCategory3,
    productosCategory4,
    productosCategory5,
    productosCategory6,
    productosCategory7,
    productosCategory8,
    productosCategory9,
    productosCategory10,
    reload
  } = useProducts();
  const [refreshing, setRefreshing] = useState(false);

  //Reload productos
  const onRefresh = React.useCallback(async () => {
    setRefreshing(true)
    await reload();
    setRefreshing(false)
  }, []);

 
  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
       <ActivityIndicator size="large" color={"#ff6900"} ></ActivityIndicator>
        <Text style={{textAlign:'center',color:'#ff6900'}}>Cargando productos...</Text>
      </View>
    );
  }else if(size(productosCategory1)===0 && size(productosCategory2)===0 && size(productosCategory3)===0 && size(productosCategory4)===0
  && size(productosCategory5)===0 && size(productosCategory6)===0 && size(productosCategory7)===0 && size(productosCategory8)===0 && size(productosCategory9)===0  ){
    return(
      <View style={styles.conatainerNot}>
        <NotProducts></NotProducts>
      </View>
    )
}

  return (
    <>
    <StatusBarCustom backgroundColor={colores.black} />
    <Search/>
    <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />} >
      <View style={{ marginTop: 10,marginBottom:100, backgroundColor:colores.white }}>
        {/**Categorias */}
        {size(productosCategory1) !== 0 &&
        <HorizontalSlider
          title="Lean Choice"
          products={productosCategory1}
        />}
        {size(productosCategory2) !== 0 &&
        <HorizontalSlider
          title="Especialidades"
          products={productosCategory2}
        />}
         {size(productosCategory3) !== 0 &&
        <HorizontalSlider
          title="Practicocina"
          products={productosCategory3}
        />}
        {size(productosCategory4) !== 0 &&
        <HorizontalSlider
          title="Linea Casera"
          products={productosCategory4}
        />}
        {size(productosCategory5) !== 0 &&
        <HorizontalSlider
          title="Marinados"
          products={productosCategory5}
        />}
        {size(productosCategory6) !== 0 &&
        <HorizontalSlider
          title="Carnes Frías"
          products={productosCategory6}
        />}
        {size(productosCategory7) !== 0 &&
        <HorizontalSlider
          title="Cortes Finos"
          products={productosCategory7}
        />}
        {size(productosCategory8) !== 0 &&
        <HorizontalSlider
          title="Linea Turi"
          products={productosCategory8}
        />}
        {size(productosCategory9) !== 0 &&
        <HorizontalSlider
          title="Frescos"
          products={productosCategory9}
        />}
        {size(productosCategory10) !== 0 &&
        <HorizontalSlider
          title="Navideños"
          products={productosCategory10}
        />}
      </View>
    </ScrollView>
    </>
  );
};


const styles = StyleSheet.create({ 
  title: {
    flex:1,
    fontSize: 14,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  costo:{
    fontSize: 15,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  text:{
    color:colores.blue
  },
  conatainerNot: {
    flex:1,
    alignSelf:"center",
    marginTop:"70%",
    color:colores.primary
  },
  iconFlex:{
    alignSelf:"center"
  }
});
