import { StackScreenProps } from "@react-navigation/stack";
import { Dimensions, ScrollView } from "react-native";
import { Image, StyleSheet, Text, View } from "react-native";
import { RootStackParams } from "../navigator/StackNavigator";
import Icon from "react-native-vector-icons/Ionicons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { API_URL_IMAGES } from "../properties/constants";
import { Button, TextInput } from "react-native-paper";
import { useState } from "react";
import { colores } from "../theme/styles";
import React from "react";

const screenHeight = Dimensions.get("window").height;

interface Props extends StackScreenProps<RootStackParams, "DetailScreen"> {}

export const DetailScreen = ({ route,navigation }: Props) => {
  const objProducts = route.params;
  const uri=`${API_URL_IMAGES}${objProducts.Imagen}`;
  const [quantity, setQuantity] = useState(1);

  
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.imgContainer}>
        <View style={styles.imgBorder}>
          <Image source={{ uri }} style={styles.posterImage} />
        </View>
      </View>

      {/**Boton regresar */}
      <View style={styles.backButton}>
        <TouchableOpacity onPress={()=> navigation.pop()}>
          <Icon color={"gray"} name={"arrow-back-outline"} size={50} />
        </TouchableOpacity>
      </View>

      <View>
        <Text style={styles.title}>{objProducts.Descripcion}</Text>
        <View style={styles.marginView}>
          <Text style={styles.titleQuantity}>Ingrese la cantidad:</Text>
          <TextInput
            style={{
              width: "100%",
              height: 40,
              marginBottom:10 
            }}
            onChangeText={(item)=>setQuantity(item)}
            mode="outlined"
            keyboardType ='number-pad'
            theme={{ colors: { primary: colores.primary }, roundness: 30 }}
          />            
        </View>
        <View style={styles.marginButton}>
              <Button
                  style={styles.button}
                  icon="cart-plus"
                  mode="contained"
                  uppercase={false}
                  onPress={() => console.log("Agregado al carito")}>
                  <Text> Agregar al carrito</Text>
              </Button>
            </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  posterImage: {
    flex: 1,
  },
  imgBorder: {
    flex: 1,
    overflow: "hidden",
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },
  button:{
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: colores.primary,
  },
  marginButton:{
    marginLeft:25,
    marginRight:25,
    paddingBottom:5
  },
  imgContainer: {
    width: "100%",
    height: screenHeight * 0.5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.24,
    shadowRadius: 7,
    elevation: 9,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },
  marginContainer: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  subTitle: {
    fontSize: 15,
    opacity: 0.8,
  },
  title: {
    fontSize: 18,
    marginTop:10,
    fontWeight: "bold",
    textAlign:'center'
  },
  backButton: {
    position: "absolute",
    zIndex: 999,
    elevation: 9,
    top: 30,
    left: 5,
  },
  marginView:{
    marginLeft:30,
    flexDirection:'row',
    width:'40%'
  },
  titleQuantity: {
    fontSize: 16,
    marginTop:20,
    marginRight:10,
    fontWeight: "bold",
    textAlign:'center'
  }
});
