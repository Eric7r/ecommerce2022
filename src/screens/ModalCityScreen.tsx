import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View } from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import { colores } from "../theme/styles";

export const ModalCityScreen = () => {

  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation<StackNavigationProp<any>>();
  const countries = ["Navojoa", "Obregon", "Hermosillo", "Culiacan"]


    useFocusEffect(
      React.useCallback(() => {
        setModalVisible(true);
        
      }, [modalVisible])
    );

    const goToHome=()=>{
      setModalVisible(false)
      navigation.push("HomeScreen");
    }
  
  

  return (
    <>
      <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {/*Select citys*/}
            <SelectDropdown
              data={countries}
              defaultButtonText="Seleccione ciudad"
              buttonStyle={{marginBottom:15,borderRadius:15}}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem, index)
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                 return selectedItem
              }}
              rowTextForSelection={(item, index) => {
                return item
              }}
            />

            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => goToHome()}
            >
              <Text style={styles.textStyle}>Aceptar</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
    </>
    
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:colores.primary
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: colores.blue,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize:18

  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
