import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Loading } from "../components/Loading";
import { Search } from "../components/Search";
import { API_URL_IMAGES } from "../properties/constants";
import { getProductById, getUserById } from "../properties/services";
import { colores } from "../theme/styles";
import Icon from "react-native-vector-icons/Ionicons";
import { Button, List, TextInput } from "react-native-paper";
import useAuth from "../hooks/useAuth";
import { StatusBarCustom } from "../components/StatusBar";
import Buy from "../components/Buy";
import SelectList from 'react-native-dropdown-select-list';

interface jsonProduct{
    "IdCatArticulos": 0,
    "Descripcion": null,
    "Codigo": null,
    "Imagen": null,
    "ciudad": null,
    "Precio": 0,
    "Stock":0,
    "Unidad":null,
    "IdDatArticulos":0,
    "IdDatPrecios":0,
    "DescripcionGeneral":null,
    "nameCategoria":""
}

const screenHeight = Dimensions.get("window").height;

export const DetailProduct = (props: any) => {

  const { route} = props;
  const { params } = route;
  const [product, setProduct] = useState<jsonProduct>();
  const [quantity, setQuantity] = useState(0);
  const { auth } = useAuth();
  const uri=`${API_URL_IMAGES}${product?.Imagen}`;
  const navigation = useNavigation();
  const [selected, setSelected] = useState("");
  //const data = [{key:'Kg',value:'Kg'},{key:'Pieza',value:'Pieza'}];
  const [precio, setPrecio] = useState(0);
  const [data,setData] = useState([{key:'Kg',value:'Kg'},{key:'Pieza',value:'Pieza'}]);
 

  //Get de lista de productos por id
  useEffect(() => {
    (async () => {
      const response = await getProductById(params.idProduct,auth?.idcity);
      setProduct(response);
      
      //Valida si tiene pesoPromedio no muestra las piezas
      if(response.CantPesoProm == '.00'){
        setData([{key:'Kg',value:'Kg'}])
        setPrecio(response.Precio);
        
      }else{
        setData([{key:'Pieza',value:'Pieza'}])
        setPrecio(parseFloat(response.CantPesoProm)*parseFloat(response.Precio));
      }
    })();
  }, [params,selected]);


  return (
    <>
        {/*<StatusBar />*/}
        {!product ? (
          <Loading/>
        ) : (
          <>
          <StatusBarCustom backgroundColor={colores.black} />
          <Search/>
          <ScrollView style={{backgroundColor:colores.primary}}>
          <View style={styles.imgContainer}>
            <View style={styles.imgBorder}>
              <Image source={{ uri }} style={styles.posterImage} />
            </View>
          </View>
    
          {/**Boton regresar */}
          <View style={styles.backButton}>
            <TouchableOpacity onPress={()=> navigation.pop()}>
              <Icon color={colores.primary} name={"arrow-undo-sharp"} size={50} />
            </TouchableOpacity>
          </View>
    
          <View style={styles.form}>
            <Text style={styles.titleCategory}>{product.nameCategoria.toUpperCase()}</Text>
            <Text style={styles.title}>{product.Descripcion}</Text>
            <View style={styles.detailRow}>
                 <Text style={styles.subtitle}>Precio:<Text style={styles.titlePrice}>${precio==null?0:precio.toFixed(2)}</Text> {data[0]?.value}</Text>
                 <Text style={styles.subtitle}>Stock:<Text style={styles.titleStock}>{product.Stock==null?0:product.Stock}</Text></Text>
            </View>

            <View > 
                <List.Section >
                  <List.Accordion
                    theme={{ colors: { primary: colores.primary} }}
                    style={{backgroundColor:colores.white}}
                    title={"Descripción del producto:"}
                    titleStyle={{fontWeight:'bold'}}
                    left={props => <List.Icon {...props} icon="information-outline"/>}>
                      <Text style={styles.textDesc}>{product.DescripcionGeneral}</Text>
                  </List.Accordion>
                </List.Section>
            </View>
            
            
            <View style={styles.marginView}>
              <Text style={styles.titleQuantity}>Ingrese la cantidad:</Text>
              <TextInput
                style={{
                  width: "100%",
                  height: 40,
                  marginBottom:10,
                  backgroundColor:colores.white 
                }}
                onChangeText={(item)=>setQuantity(item)}
                placeholder="0.00"
                mode="outlined"
                keyboardType ='decimal-pad'
                theme={{ colors: { primary: colores.primary }, roundness: 30 }}
              /> 
            </View>
            
            <View style={styles.marginButton}>
             <Buy product={product} quantity={quantity} tipo={data[0]?.value} precio={precio}/>
            </View>
          </View>

        </ScrollView>
        </>        
        )}
    </>
  );
};

const styles = StyleSheet.create({
  posterImage: {
    flex: 1,
  },
  imgBorder: {
    flex: 1,
    overflow: "hidden",
    borderBottomEndRadius: 40,
    borderBottomStartRadius: 40
  },
  imgContainer: {
    width: "100%",
    height: screenHeight * 0.35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.24,
    shadowRadius: 7,
    elevation: 9,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },
  marginContainer: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  subTitle: {
    fontSize: 15,
    opacity: 0.8,
  },
  title: {
    fontSize: 18,
    marginTop:10,
    fontWeight: "bold",
    textAlign:'center'
  },
  titleCategory: {
    fontSize: 22,
    marginTop:10,
    color:colores.blue,
    fontWeight: "bold",
    textAlign:'center'
  },
  titleDesc: {
    fontSize: 16,
    marginTop:5,
    fontWeight: "bold",
    textAlign:'center'
  },
  subtitle:{
    fontSize:16,
    fontWeight: "bold",
    color:"gray"
  },
  titlePrice: {
    fontSize: 16,
    marginTop:10,
    fontWeight: "bold",
    textAlign:'center',
    color:colores.primary
  },
  titleStock: {
    fontSize: 16,
    marginTop:10,
    fontWeight: "bold",
    textAlign:'center',
    color:colores.primary
  },
  backButton: {
    position: "absolute",
    zIndex: 999,
    elevation: 9,
    top: 5,
    left: 5,
  },
  button:{
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: colores.primary,
  },
  textButton:{
    fontSize: 18, color: "white" 
  },
  marginView:{
    marginLeft:30,
    flexDirection:'row',
    width:'40%'
  },
  marginViewTipo:{
    marginLeft:30,
    flexDirection:'row',
    width:'50%',
    marginBottom:10
  },
  titleQuantity: {
    fontSize: 14,
    marginTop:20,
    marginRight:10,
    fontWeight: "bold",
    textAlign:'center'
  },
  marginButton:{
    marginLeft:25,
    marginRight:25,
    paddingBottom:5
  },
  detailRow:{
    flexDirection:'row',
    justifyContent:'space-around'
  },
  descriptionProd:{
    flexDirection:'row',
    alignSelf:'center',
    marginHorizontal:30
  },
  textDesc:{
    fontSize:15,
    fontWeight:'bold',
    color:colores.blue,
    marginEnd:20
  },
  acordeon:{
    textAlign:'center'
  },
  form:{
    width:'100%',
    height:screenHeight*0.6,
    backgroundColor:'white',
    bottom:0,
    marginTop:20,
    borderTopLeftRadius:40,
    borderTopRightRadius:40,
    padding:10
  }
});
