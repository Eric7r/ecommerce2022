import React from 'react'
import PdfReader from 'rn-pdf-reader-js';

export default function RecetasScreen() {
  return (
    <PdfReader
    source={{
    uri: "https://ecommercerest.kowi.com.mx/recetas/RECETARIONAVIDE%C3%91O2022-KOWI.pdf",
    }}
/>
  )
}
