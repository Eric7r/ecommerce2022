/*import { createContext, useReducer } from "react";
import { AuthReducer } from "./AuthReducer";

//Definir como luce
export interface AuthState{
    isLoggedIn:boolean;
    username?:string;
}

//Estado inicial
export const authInitialState: AuthState={
    isLoggedIn:false,
    username:undefined
}

//Lo usuaremos para decirle a react como luce y que expone el context
export interface AuthContextProps{
    authState:AuthState;
    signIn:()=>void;
    logout:()=>void;
}

//Crear el contexto
export const AuthContext=createContext({} as AuthContextProps);

//Componenete provedor del estado
export const AuthProvider=({children}:any)=>{

    const [authState, dispatch] = useReducer(AuthReducer, authInitialState);
    
    const signIn=()=>{
        dispatch({type:'signIn'});
    }

    const logout=()=>{
        dispatch({type:'logout'})
    }
    console.log(authInitialState);
    

    return(
        <AuthContext.Provider value={{
            authState,
            signIn,
            logout
        }}>
            {children}
        </AuthContext.Provider>
    )

}*/

import { createContext } from 'react';

const AuthContext=createContext({
    auth:undefined,
    login: ()=> null,
    logout: ()=> null,
    recovery:()=>null
})

export default AuthContext;