import { AuthState } from './AuthContext';

type AuthAction=
|{ type :'signIn' }
|{ type :'logout' }
//Genera estado
export const AuthReducer=(state:AuthState,action:AuthAction): AuthState =>{

    switch (action.type) {
        case 'signIn':
            return{
                ...state,
                isLoggedIn:true,       
            }
        case 'logout':
            return{
                ...state,
                isLoggedIn:false,
            }
    
        default:
            return state;
    }
}