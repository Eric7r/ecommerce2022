import { StyleSheet } from "react-native";

export const colores={
    primary:'#ff6900',
    black:'black',
    white:'#fff',
    blue:'#267ED6',
    danger:"#b30000",
    warning:'#ffcc00',
    green:'green'
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      //height:300,
      flexDirection: "column"
    },
    bloque1Login:{
        flex:1.3
    },
    bloque2Login:{
        flex:0.8,
        backgroundColor:"white"
    },
    title1Login:{
        fontSize: 18,
        alignSelf: "center", 
        color:"black",
        opacity:0.8
    },
    title2Login:{
        fontSize: 14,
        alignSelf: "center" 
    },
    inputLogin:{
        marginTop: 3,
        width: "80%",
        marginLeft: "10%",
        marginRight: "10%",
        height: 50
    },
    btnIniciarSesion:{
        marginTop: 10,
        height:45,
        justifyContent:'center',
        width: "80%",
        marginLeft: "10%",
        marginRight: "10%",
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor: "#ff6900",
    },
    btnRegistrarse:{
        marginTop: 5,
        height:45,
        justifyContent:'center',
        width: "80%",
        marginLeft: "10%",
        marginRight: "10%",
        borderRadius: 15,
        borderWidth: 1
    },
    tituloHeader:{
        flexDirection: "row",
        justifyContent: "flex-end",
        marginTop: 10,
        marginRight: 10
    },
    paddingScreen:{
       paddingTop:50
    },
    btnNoLogin:{
        marginTop: 10,
        height:45,
        justifyContent:'center',
        width: "80%",
        marginLeft: "10%",
        marginRight: "10%",
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor: colores.blue,
    }
  });

  export default styles;