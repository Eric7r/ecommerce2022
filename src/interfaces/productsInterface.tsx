// Generated by https://quicktype.io

export interface ProductResponse {
  IdCatArticulos: number;
  Descripcion: string;
  Codigo: null | string;
  Imagen: null | string;
  ciudad: string;
  Precio: number | null;
}

export enum Categoria {
  SinFamilia = "SIN FAMILIA",
}
