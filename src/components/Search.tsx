import { useNavigation, useRoute } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { useState } from "react";
import { StyleSheet, Text, View, Animated, Keyboard } from "react-native";
import { Searchbar } from "react-native-paper";
import { updateSearchHistoryApi } from "../api/search";
import { colores } from "../theme/styles";
import {
  AnimatedIcon,
  inputAnimation,
  inputAnimationWidth,
  animatedTransition,
  animatedTransitionReset,
  arrowAnimation,
} from "./SearchAnimation";
import { SearchHistory } from "./SearchHistory";

export const Search = (props:any) => {
  const {currentSearch}=props;
  const [searchQuery, setSearchQuery] = useState(currentSearch || "");
  const navigation=useNavigation<StackNavigationProp<any>>();
  const [showHistory, setShowHistory] = useState(false);
  const [containerHeight, setContainerHeight] = useState(0);
  const onChangeSearch = (query: any) => setSearchQuery(query);
  const route=useRoute();
  //console.log(route.name);
  

  const openSearch = () => {
    //console.log("Buscador activo");
    animatedTransition.start();
    setShowHistory(!showHistory);
  };

  const closeSearch = () => {
    //console.log("Buscador cerrado");
    animatedTransitionReset.start();
    Keyboard.dismiss();
    setShowHistory(!showHistory);
  };

  const onSearch = async(reuseSearch:any) => {
    const isReuse= typeof reuseSearch === "string";

    closeSearch();
    
    !isReuse && (await updateSearchHistoryApi(searchQuery));

    
    if(route.name==="Favorites" || route.name==="cart"){
      navigation.navigate("SearchProduct", {search: isReuse ? reuseSearch: searchQuery})
    }else{
      navigation.push("SearchProduct", {search: isReuse ? reuseSearch: searchQuery})
    }
    
  };

  return (
    <View style={styles.container} onLayout={(e)=>setContainerHeight(e.nativeEvent.layout.height)}>
      <View style={styles.containerInput}>
         {/*Icono back busqueda*/ }
        <AnimatedIcon
          name="arrow-left"
          size={20}
          style={[styles.backArror, arrowAnimation]}
          onPress={() => closeSearch()}
        />
         {/*Animacion del boton de busqueda*/ }
        <Animated.View style={[inputAnimation, { width: inputAnimationWidth }]}>
          <Searchbar
            placeholder="Buscar producto"
            onFocus={openSearch}
            onChangeText={onChangeSearch}
            onSubmitEditing={onSearch}
            value={searchQuery}
          />
        </Animated.View>
      </View>
      {/*Historial de busqueda*/ }
      <SearchHistory showHistory={showHistory} containerHeight={containerHeight} onSearch={onSearch}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colores.black,
    paddingVertical: 10,
    paddingHorizontal: 20,
    zIndex: 1,
  },
  containerInput: {
    position: "relative",
    alignItems: "flex-end",
  },
  backArror: {
    position: "absolute",
    left: 0,
    top: 15,
    color: "white",
  },
});
