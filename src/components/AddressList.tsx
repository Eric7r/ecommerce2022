import { View, Text, StyleSheet, Alert } from "react-native";
import React from "react";
import { map } from "lodash";
import { colores } from "../theme/styles";
import { Button, IconButton } from "react-native-paper";
import { deleteAddressApi } from "../api/address";

export default function AddressList(props: any) {
  const { addresses,setReloadAddress } = props;

  const deleteAddressAlert=(address:any)=>{
     Alert.alert(
        "Eliminando dirección",
        `¿Estas seguro que quieres eliminar la dirección ${address.Direccion}?`,
        [
            {text:"NO"},
            {text:"SI", onPress:()=>deleteAddress(address.IdCatCteDirEnv)
            }
        ],
        {cancelable:false}
     )
  }

  const deleteAddress= async(idAddress:any)=>{
    try {
        await deleteAddressApi(idAddress);
        setReloadAddress(true);
    } catch (error) {
        console.log(error);
    }
  }

  return (
    <View style={styles.container}>
      {map(addresses, (address: any, index: any) => (
        <View key={address.IdCatCteDirEnv} style={styles.address}>
          <Text style={styles.titleAddress}>Dirección #{index + 1}</Text>
          <Text style={styles.subtitle}>Dirección:</Text><Text>{address.Direccion}</Text>
          <View style={styles.blockLine}>
            <Text style={styles.subtitle}>Número exterior: </Text><Text>{address.Num_Ext==null || address.Num_Ext=="" ?"S/N":address.Num_Ext} </Text>
            <Text style={styles.subtitle}>Número exterior: </Text><Text>{address.Num_Int==null || address.Num_Int=="" ?"S/N":address.Num_Int} </Text>
          </View>
          <Text style={styles.subtitle}>Colonia:</Text><Text>{address.Colonia}</Text>
          <View style={styles.blockLine}>
            <Text style={styles.subtitle}>Ciudad: </Text><Text>{address.Ciudad==null || address.Ciudad=="" ?"No disponible":address.Ciudad} </Text>
            <Text style={styles.subtitle}>Código postal:</Text><Text>{address.Codigo_Postal}</Text>
          </View>
          <Text style={styles.subtitle}>Teléfono:</Text><Text>{address.Telefono==null?'No disponible':address.Telefono}</Text>
          <View style={styles.actions}>
            {/**<Button mode="contained" color={colores.blue}>Editar</Button>/** */}
            <IconButton icon="delete-forever-outline" color={colores.white} size={22} style={styles.btnDelete} onPress={()=>deleteAddressAlert(address)} />
            {/**<Button mode="contained" color={colores.danger} onPress={()=>deleteAddressAlert(address)}>Eliminar</Button>/** */}
          </View>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
  },
  address: {
    borderWidth: 2 ,
    borderRadius: 5,
    borderColor: "#ddd",
    paddingHorizontal: 15,
    paddingVertical: 15,
    marginBottom: 15,
  },
  titleAddress: {
    fontSize: 17,
    fontWeight: "bold",
    alignSelf: "center",
    marginBottom: 5,
    color:colores.primary
  },
  subtitle:{
    fontSize:15,
    fontWeight:'bold',
    color:colores.blue
  },
  blockLine:{
    flexDirection:'row'
  },
  btnDelete:{
    backgroundColor:colores.danger,
    borderRadius:5,
  },
  actions:{
    flexDirection:"row",
    justifyContent:"flex-end"
  }
});
