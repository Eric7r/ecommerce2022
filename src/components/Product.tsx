import { View, Text, StyleSheet, Image, TextInput } from "react-native";
import React from "react";
import { API_URL_IMAGES } from "../properties/constants";
import { colores } from "../theme/styles";
import { Button, IconButton} from "react-native-paper";
import { decreaseProductCartApi, deleteProductCartApi, increaseProductCartApi } from "../api/cart";
import useAuth from "../hooks/useAuth";

export default function Product(props: any) {
  const { product, setReloadCart } = props;
  const {auth}= useAuth();

  const deleteProductCart= async ()=>{
    const response= await deleteProductCartApi(product.IdCatArticulos,auth?.iduser);
    if(response) setReloadCart(true);
    
  }

  const increaseProductCart=async ()=>{
    const response= await increaseProductCartApi(product.IdCatArticulos,auth?.iduser);
    if(response) setReloadCart(true);
  }

  const decreaseProductCart=async ()=>{
    const response= await decreaseProductCartApi(product.IdCatArticulos,auth?.iduser);
    if(response) setReloadCart(true);
  }

  return (
    <View style={styles.product}>
      <View style={styles.containerImage}>
        <Image
          style={styles.image}
          source={{ uri: `${API_URL_IMAGES}${product.Imagen}` }}
        />
      </View>

      <View style={styles.info}>
        <View>
          <Text style={styles.name} numberOfLines={3} ellipsizeMode="tail">
            {product.Descripcion}
          </Text>
        </View>

        <View style={styles.price}>
          <Text style={styles.priceText}><Text style={styles.priceTextUnidad}>Precio:</Text>$ {product.Precio==null?0:product.Precio.toFixed(2)}</Text>
          <Text style={styles.priceTextUnidad}> Unidad:<Text style={styles.txtUnidad}>{product.tipoUnidad}</Text></Text>
        </View>

        <View style={styles.btnContainer}>
          <View style={styles.selectQuantity}>
            <IconButton icon="minus" color={colores.white} size={19} style={styles.btnQuantityDecrease} onPress={decreaseProductCart}/>
            <TextInput style={styles.inputQuantity} value={product.quantity.toString()} />
            <IconButton icon="plus" color={colores.white} size={19} style={styles.btnQuantity} onPress={increaseProductCart} />
            <IconButton icon="delete-forever-outline" color={colores.white} size={19} style={styles.btnDelete} onPress={deleteProductCart} />
          </View>
        </View>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  product: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 2,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  containerImage: {
    width: "40%",
    height: 120
  },
  image: {
    height: "100%",
    borderRadius:18
  },
  info: {
    padding: 10,
    width: "60%",
    justifyContent: "space-between",
  },
  name: {
    fontSize: 14,
    fontWeight:"bold",
  },
  price: {
    flexDirection: "row",
    alignItems: "flex-end",
  },
  priceText:{
    color: colores.primary,
    fontSize: 14,
    fontWeight:"bold"
  },
  priceTextUnidad:{
    color: colores.black,
    fontSize: 14,
    fontWeight:"bold"
  },
  txtUnidad:{
    color: colores.blue,
    fontSize: 14,
    fontWeight:"bold"
  },
  btnContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    position: "relative",
    width: "100%",
  },
  selectQuantity: {
    flexDirection: "row",
    alignItems: "center"
  },
  btnQuantity:{
    backgroundColor:colores.blue,
    borderRadius:5,
    margin:0,
    
  },
  btnQuantityDecrease:{
    backgroundColor:colores.warning,
    borderRadius:5,
    margin:0,
    
  },
  inputQuantity:{
    paddingHorizontal:10,
    fontSize:16
  },
  btnDelete:{
    backgroundColor:colores.danger,
    borderRadius:5,
    margin:0,
    marginHorizontal:15
  }
});
