import { View, Text, StyleSheet, Image, ScrollView } from "react-native";
import React from "react";
import { colores } from "../theme/styles";
import { API_URL_IMAGES } from "../properties/constants";
import { Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

export default function OrderHistory(props: any) {
  const { pedidos } = props;
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      {/* Datos generales de la venta */}
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}>
        <View>
          <Text style={styles.name}>{pedidos.Fecha_Pago} </Text>
          <Text style={styles.nameBlue}>{pedidos.Status}</Text>
        </View>

        {pedidos.isFactura && !pedidos.SolicitudFE && (
          <View>
            <Button
              style={styles.btnFactura}
              // icon={"unicorn"}
              mode="outlined"
              color={colores.white}
              uppercase={false}
              onPress={() =>
                navigation.navigate("factura", { idVenta: pedidos.IdDatVentas })
              }>
              <Text style={[{ fontSize: 10 }]}>Solicitar factura</Text>
            </Button>
          </View>
        )}
        {pedidos.SolicitudFE && (
          <View>
            {/* <Button
              style={styles.btnSuccess}
              // icon={"unicorn"}
              mode="text"
              color={colores.green}
              uppercase={false}
              onPress={() =>
                navigation.navigate("factura", { idVenta: pedidos.IdDatVentas })
              }>
              <Text style={[{ fontSize: 10 }]}>Factura solicitada</Text>
            </Button> */}
            <Text style={styles.nameGreen}>Factura solicitada</Text>
          </View>
        )}
      </View>

      {/* Detalle de pedido */}
      {pedidos.detalles_ventas.map((item: any, index: any) => (
        <View style={styles.row} key={index}>
          <Image
            source={{ uri: `${API_URL_IMAGES}${item.Imagen}` }}
            style={styles.image}
          />
          <View style={{ flexDirection: "column", paddingLeft: 8 }}>
            <Text style={styles.namePrimary}>{item.DesCorta}</Text>
            <Text style={styles.name}>
              Precio:
              <Text style={styles.namePrimary}> ${item.Precio}</Text>
            </Text>
            <Text style={styles.name}>
              Cantidad:
              <Text style={styles.nameBlue}>
                {" "}
                {item.Cantidad / item.CantPesoProm}
              </Text>
            </Text>
            <Text style={styles.name}>Importe ${item.Importe}</Text>
          </View>
        </View>
      ))}

      {/* Totales */}
      <View style={{ alignItems: "flex-end" }}>
        <Text style={styles.name}>
          Costo envio:
          <Text style={styles.namePrimary}> ${pedidos.Costo_Envio}</Text>
        </Text>
        <Text style={styles.name}>
          Total:
          <Text style={styles.namePrimary}> ${pedidos.Importe_Pago}</Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colores.white,
    borderRadius: 20,
    marginHorizontal: 15,
    marginTop: 10,
    shadowColor: "#000",
    padding: 16,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
    padding: 3,
  },
  image: {
    width: "25%",
    height: "100%",
    borderRadius: 10,
  },
  name: {
    fontSize: 13,
    fontWeight: "bold",
  },
  nameBlue: {
    fontSize: 13,
    fontWeight: "bold",
    color: colores.blue,
  },
  nameGreen: {
    fontSize: 10,
    fontWeight: "bold",
    color: colores.green,
  },
  namePrimary: {
    fontSize: 13,
    fontWeight: "bold",
    color: colores.primary,
  },
  btnFactura: {
    borderRadius: 15,
    backgroundColor: colores.black,
    alignSelf: "center",
  },
  btnSuccess: {
    alignSelf: "flex-start",
    padding: 0,
  },
});
