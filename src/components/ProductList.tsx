import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from "react-native";
import React from "react";
import { map } from "lodash";
import { ScrollView } from "react-native-gesture-handler";
import { API_URL_IMAGES } from "../properties/constants";
import { colores } from "../theme/styles";
import { Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import useAuth from "../hooks/useAuth";

export default function ProductList(props: any) {
  const { products } = props;
  const navigation = useNavigation<StackNavigationProp<any>>();
  const {auth} = useAuth();

  return (
    <ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>
      <Text style={styles.title}>Resultados encontrados</Text>
      {map(products, (product) => (
        <TouchableWithoutFeedback key={product.IdCatArticulos}>
          <View style={styles.product}>
            <View style={styles.containerImage}>
              <Image
                style={styles.image}
                source={{ uri: `${API_URL_IMAGES}${product.Imagen}` }}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.name} numberOfLines={3} ellipsizeMode="tail">
                {product.Descripcion}
              </Text>
              <View style={styles.price}>
                <Text style={styles.textPrice}>
                  $ {product.Precio == null ? 0 : product.Precio}
                </Text>
              </View>
              <Button
                style={styles.button}
                icon="eye-plus-outline"
                mode="contained"
                uppercase={false}
                onPress={() =>
                  navigation.navigate("DetailProduct", {
                    idProduct: product.IdCatArticulos
                  })
                }
              >
                <Text> Ver producto</Text>
              </Button>
            </View>
          </View>
        </TouchableWithoutFeedback>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    paddingBottom:Dimensions.get("window").height*0.12,
  },
  title: {
    fontSize: 20,
    marginBottom:15,
    marginTop:5,
    fontWeight: "bold",
    alignSelf: "center",
    color:colores.primary
  },
  product: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "white",
    borderRadius: 20,
    //padding: 10,
    marginLeft: 15,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  image: {
    height: "100%",
    resizeMode: "contain",
  },
  containerImage: {
    width: "40%",
    height: 120,
    backgroundColor: "#f0f0f0",
    padding: 5,
    borderRadius:20
  },
  info: {
    padding: 10,
    width: "60%",
  },
  name: {
    fontSize: 15,
    fontWeight: "bold",
  },
  price: {
    flexDirection: "row",
    alignSelf: "flex-start",
    marginTop: 5,
  },
  textPrice: {
    color: colores.primary,
    fontSize: 16,
    fontWeight: "bold",
  },
  button: {
    position: "absolute",
    bottom: 10,
    left: 10,
    right: 0,
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: colores.blue,
  },
});
