import React from 'react'
import { SafeAreaView, StatusBar } from 'react-native'

export const StatusBarCustom = (props:any) => {
    
    const { backgroundColor }= props;

  return (
    <>
    <StatusBar barStyle="light-content"/>
      <SafeAreaView style={{backgroundColor:backgroundColor}}/>
    </>
  )
}
