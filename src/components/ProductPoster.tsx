import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Button } from "react-native-paper";
import { ProductResponse } from "../interfaces/productsInterface";
import { API_URL_IMAGES } from "../properties/constants";
import { colores } from "../theme/styles";

interface Props {
  productos: ProductResponse;
  height?: number;
  width?: number;
}

export const ProductPoster = ({ productos, height = 420, width = 600 }: Props) => {
  
  const uri=`${API_URL_IMAGES}${productos.Imagen}`;
  const navigation = useNavigation<StackNavigationProp<any>>();

  return (
    <View
    style={{ 
      width,
      height,
      marginHorizontal: 5,
      paddingBottom:20,
      paddingHorizontal:3 
    }}

    >
      <View style={styles.contenedor}>
        <Image source={{ uri }} style={styles.image} />
        <View style={{padding:5}}>
          <Text style={{fontSize: 15}} >{productos.Descripcion}</Text>
          <Text style={{fontSize: 14, color:colores.primary}}>$ {productos.Precio==null?0:productos.Precio}</Text>
          <Button
            style={styles.button}
            icon="import"
            mode="contained"
            uppercase={false}
            onPress={()=>navigation.navigate('DetailProduct', { idProduct: productos.IdCatArticulos })}
          >
            <Text style={styles.textButton}> Ver producto</Text>
          </Button>
        </View>
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 1,
    borderRadius: 18,
  },
  imgContainer: {
    flex: 1,
    borderRadius: 18,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.24,
    shadowRadius: 7,
    elevation: 8,
  },
  contenedor:{
    flex: 1,
    backgroundColor:'white',
    borderRadius: 20,
    padding: 0,
    marginLeft:15,
    marginBottom:10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  button:{
    width: "100%",
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: colores.blue,
  },
  textButton:{
    fontSize: 11, color: "white" 
  }
});
