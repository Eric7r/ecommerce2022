import React from 'react'
import { FlatList, Text, View } from 'react-native';
import { ProductResponse } from '../interfaces/productsInterface';
import { colores } from '../theme/styles';
import { ProductPoster } from './ProductPoster';

interface Props{
    title?:string;
    products:ProductResponse[];
    color?:string;
}

export const HorizontalSlider = ({title,products,color='black'}:Props) => {
  return (
  <View>
     <Text style={{ fontSize: 20, fontWeight: "bold",marginLeft:15,paddingBottom:10,color:colores.primary }}>{title}</Text>
     <View>
      <FlatList
          data={products}
          renderItem={({ item }: any) => (
              <ProductPoster productos={item} width={220} height={240} />
          )}
          keyExtractor={(item) => item.IdCatArticulos.toString()}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
      />
     </View>
  </View>
  
  )
  
}
