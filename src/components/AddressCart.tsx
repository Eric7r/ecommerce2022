import { View, Text, StyleSheet, TouchableWithoutFeedback, Dimensions } from "react-native";
import React, { useEffect } from "react";
import { map } from "lodash";
import { colores } from "../theme/styles";
import { Loading } from "./Loading";
import { Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";


export default function AddressCart(props: any) {
  const { addresses, selectedAddress, setSelectedAddress, products } = props;
  const navigation = useNavigation();
  

  useEffect(() => {
    addresses && setSelectedAddress(addresses[0]);
  }, [addresses]);

  return (
    <View style={styles.container}>
      
      <View style={styles.rowDireccion}>
        <Text style={styles.containerTitle}>Dirección de envio: </Text>
        <Button 
        style={styles.buttonDirection}
        icon="plus-circle-outline"
        mode="contained"
        uppercase={false}
        onPress={() => navigation.navigate("addaddress" as never)}>
          <Text style={styles.dirText}>Agregar dirección</Text>
        </Button>
      </View>

      {!addresses && <Loading />}
      {map(addresses, (address: any) => (
        <TouchableWithoutFeedback
          key={address.IdCatCteDirEnv}
          onPress={() => setSelectedAddress(address)}
        >
          <View
            style={[
              styles.address,
              address.IdCatCteDirEnv == selectedAddress?.IdCatCteDirEnv &&
                styles.checked,
            ]}>

            <Text style={styles.subtitle}>Dirección:</Text>
            <Text>{address.Direccion}</Text>
             
            <View style={styles.blockLine}>
              <Text style={styles.subtitle}>Número interior: </Text>
              <Text>
                {address.Num_Int == null || address.Num_Int == ""
                  ? "S/N"
                  : address.Num_Int}{" "}
              </Text>
              <Text style={styles.subtitle}>Número exterior: </Text>
              <Text>
                {address.Num_Ext== null || address.Num_Ext== ""
                  ? "S/N"
                  : address.Num_Ext}{" "}
              </Text>
            </View>

            <Text style={styles.subtitle}>Colonia:</Text>
            <Text>{address.Colonia}</Text>
            <Text style={styles.subtitle}>Teléfono:</Text>
            <Text>
              {address.Telefono == null ? "No disponible" : address.Telefono}
            </Text>
            
            <View style={styles.blockLine}>
              <Text style={styles.subtitle}>Ciudad: </Text>
              <Text>
                {address.Ciudad == null || address.Ciudad == ""
                  ? "No disponible"
                  : address.Ciudad}{" "}
              </Text>
              <Text style={styles.subtitle}>Código postal:</Text>
              <Text>{address.Codigo_Postal}</Text>
            </View>

          </View>
        </TouchableWithoutFeedback>
      ))}
      <View style={styles.containerPayment}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
  },
  containerPayment: {
    marginBottom: 5,
  },
  containerTitle: {
    paddingBottom: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
  address: {
    borderWidth: 1.0,
    borderRadius: 5,
    borderColor: "gray",
    padding: 15,
    marginBottom: 10,
  },
  title: {
    fontWeight: "bold",
    fontSize: 14,
  },
  subtitle: {
    fontSize: 14,
    fontWeight: "bold",
    color: colores.blue,
  },
  blockLine: {
    flexDirection: "row",
  },
  checked: {
    borderColor: colores.blue,
    backgroundColor: "#0098d330",
  },
  rowDireccion: {
    flexDirection: "row"
  },
  buttonDirection:{
    borderRadius: 15,
    backgroundColor: colores.blue,
    marginBottom:10,
    marginTop:-5,
    marginHorizontal:"5%",
  },
  dirText:{
    fontSize:13
  }
});
