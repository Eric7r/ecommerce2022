import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import { Alert, Text, View,Platform, StyleSheet, ToastAndroid, TouchableOpacity } from "react-native";
import { ActivityIndicator, List, TextInput } from "react-native-paper";
import useAuth from "../hooks/useAuth";
import { deleteUserApp, getUserById } from "../properties/services";
import { colores } from "../theme/styles";
import { Header } from 'react-native-elements';
import Toast from "react-native-root-toast";
import Icon from "react-native-vector-icons/FontAwesome";

export default function Menu() {
  const { logout,auth } = useAuth();
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState<any>(null);
  const navigation = useNavigation<StackNavigationProp<any>>();
  const idusuario=auth?.iduser;
  

  useEffect(() => {
    (async () =>{
      const response = await getUserById(idusuario);
      setUserData(response);
      setLoading(false);
    })()
  }, [])
  
//console.log(userData?.username);

  const logoutAccount = () => {
    Alert.alert(
      userData?.username=='kowi'?"Volver al inicio":"Cerrar sesión",
      userData?.username=='kowi'?"Vuelve al inicio y procede a hacer tu registro":"¿Estas seguro de que quieres salir de tu cuenta?",
      [
        {
          text: "No",
        },
        { text: "Si", onPress: logout },
      ],
      { cancelable: false }
    );
  };

   {/*Elimina usuario de la app foreva*/}
   const deleteUser = () => {
    Alert.alert(
      "Eliminar usuario",
      "¿Estas seguro de que quieres eliminar cuenta?",
      [
        {
          text: "No",
        },
        { text: "Si", onPress: () => confirmationDelete() },
      ],
      { cancelable: true }
    );
  };

   {/*Confirmacion modal delete usuario*/}
  const confirmationDelete = () => {
    Alert.alert(
      "Su cuenta sera eliminada permanentemente",
      "¿Esta seguro de realizar esta acción?",
      [
        {
          text: "NO",
        },
        { text: "SI", onPress: ()=>functionDelete()},
      ],
      { cancelable: true }
    );
  };

  const functionDelete = async () => {
    try {

      const response = await deleteUserApp(auth);
      
      if (response.status==200) {
        logout();
        
      }else{
        Platform.OS === 'android'?
        ToastAndroid.show(response.msg, ToastAndroid.LONG):
        Toast.show(response.msg, { position: Toast.positions.CENTER, });
      }
      
    } catch (error) {
      console.log(error);
      
    }
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

  return (
    <>
    {!userData ? (
        <View
          style={{ flex: 1, justifyContent: "center", alignContent: "center",paddingTop:300 }}
        >
         <ActivityIndicator size="large" color={"#ff6900"} ></ActivityIndicator>
          <Text style={{textAlign:'center',color:'#ff6900'}}>Cargando perfil...</Text>
        </View>
        ) : (
      <View>
        
        <View style={{top:0}}>
        {(Platform.OS === 'android') &&
          <View style={styles.headerContainer}>
            {/* <TouchableOpacity style={{marginLeft:10}}>
              <Text style={styles.backButtonText}><Icon name="bars" size={30} color="white" /></Text>
            </TouchableOpacity> */}
            <Text style={styles.headerTitle}>Bienvenido {userData?.username}</Text>
          </View>
        }
          {(Platform.OS === 'ios') &&
          <Header
            backgroundColor={colores.black}
            leftComponent={
              <TextInput.Icon
                style={{ marginTop:25, marginLeft: 30 }}
                name="menu"
                color="white"
                size={35}
              />
            }
            centerComponent={{ text: "Bienvenido, "+userData?.username, style: styles.heading }}
            />
          }
        </View>
        

        <List.Section style={styles.paddingList}>
          {userData?.username!=='kowi' && (
          <List.Item
            title="Mis direcciones"
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description="Administra tus direcciones de envio"
            left={(props) => <List.Icon {...props} icon="map-marker"  color={colores.primary}/>}
            onPress={()=>navigation.navigate("address")}
          />
          )}
          {userData?.username!=='kowi' && (
          <List.Item
            title="Mis pedidos"
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description="Historial de pedidos realizados"
            left={(props) => <List.Icon {...props} icon="order-bool-ascending-variant"  color={colores.primary}/>}
            onPress={()=>navigation.navigate("orders")}
            
          />
          )}
          <List.Item
            title="Dudas y aclaraciones"
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description="Información extra"
            left={(props) => <List.Icon {...props} icon="chat-question" color={colores.primary}/>}
            onPress={()=>navigation.navigate("dudas")}
          />

          <List.Item
            title="Recetas y preparaciones"
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description="Recetas"
            left={(props) => <List.Icon {...props} icon="food-fork-drink" color={colores.primary}/>}
            onPress={()=>navigation.navigate("recetas")}
          />
             <List.Item
            title="Eliminar cuenta"
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description="Borrar cuenta"
            left={(props) => <List.Icon {...props} icon="delete" color={colores.danger}/>}
            onPress={deleteUser}
          />
          
          <List.Item
            title={userData?.username=='kowi'?"Regístrate":"Cerrar sesión"}
            titleStyle={{color:colores.black, fontSize:15, fontWeight:'bold'}}
            description={userData?.username=='kowi'?"Vuelve al inicio y realiza tu registro":"Cierra esta sesión e inicia con otra"} 
            left={(props) => <List.Icon {...props} icon="logout"  color={colores.blue}/>}
            onPress={logoutAccount}
          />
        </List.Section>
      </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  heading: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginTop: Platform.OS === 'android'?20:10,
    marginBottom:10
  },
  paddingList:{
    padding:5
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colores.black,
    height: 70,
    paddingHorizontal: 16,
    elevation: 2
  },
  backButtonText: {
    color: '#000',
    fontSize: 16,
  },
  headerTitle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color:colores.white
  }
})


