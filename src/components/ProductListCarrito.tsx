import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React, { useEffect } from "react";
import { map } from "lodash";
import { getProductById } from "../properties/services";
import { Loading } from "./Loading";
import Product from "./Product";
import { colores } from "../theme/styles";
import useAuth from "../hooks/useAuth";
import Icon from "react-native-vector-icons/Ionicons";
import { IconButton } from "react-native-paper";

export default function ProductListCarrito(props: any) {
  const { cart, products, setProducts, setReloadCart, setTotalPayment } = props;
  const {auth}= useAuth();

  useEffect(() => {
    (async () => {
      setProducts(null);
      const productTemp = [];
      let totalPaymentTemp=0;
      //console.log(cart);
      
      for await (const product of cart) {
        const response = await getProductById(product.idProduct,auth?.idcity);
        response.quantity = product.quantity;
        response.tipoUnidad = product.tipo;
        response.Precio = product.price;
        productTemp.push(response);

        //totalPaymentTemp += response.Precio * response.quantity;
        totalPaymentTemp += product.price * response.quantity;
      }
      setProducts(productTemp);
      setTotalPayment(totalPaymentTemp);

    })();
  }, [cart]);

  return (
    <View>
      {/*Valida carrito vacio*/}
      { products?.length == 0 ? 
        (<View style={styles.titleNotCart}>
          <View >
            <IconButton icon="pig-variant" color={colores.primary} size={60} style={styles.iconFlex}/>
          </View>
          <Text style={styles.title}>No hay productos en el carrito</Text>
        </View>) 
        :((<Text style={styles.title}>Carrito de compras</Text>)
        ) 
      }
      
      {/*Carga el loading cuando trae algo el carrito*/}
      {!products ? (<Loading />) : (
        map(products, (product:any) => 
          <Product  key={product.IdCatArticulos} product={product} setReloadCart={setReloadCart}/>
        )
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    flex:1,
    fontSize: 18,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  costo:{
    fontSize: 15,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  text:{
    color:colores.blue
  },
  titleNotCart: {
    flex:1,
    alignSelf:"center",
    marginTop:"60%",
    color:colores.primary
  },
  iconFlex:{
    
    alignSelf:"center"
  }
});
