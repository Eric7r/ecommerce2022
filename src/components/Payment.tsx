import {
  View,
  Text,
  StyleSheet,
  ToastAndroid,
  Modal,
  Alert,
  Platform,
  Dimensions,
} from "react-native";
import React, { useCallback, useEffect, useState } from "react";
import { Button, TextInput } from "react-native-paper";
import { colores } from "../theme/styles";
import { getCentroVenta, getComparePriceCart, registerCart, validateAddress } from "../properties/services";
import useAuth from "../hooks/useAuth";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import Toast from "react-native-root-toast";
import { deleteAllCart } from "../api/cart";

export default function Payment(props: any) {
  const { products, selectedAddress, totalPayment,citySelected,comentario,costoEnvio,setCostoEnvio } = props;
  const { auth } = useAuth();
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation<StackNavigationProp<any>>();
  const [newPayment, setNewPayment] = useState(0);
  const [loadingPayment, setLoadingPayment] = useState(false);

 
  //Formulario general del carrito
  const [form, setForm] = useState({
    nombre_tarjeta: "",
    numero_tarjeta: "",
    mes: "",
    año: "",
    cvv: "",
    lst_products: products,
    id_address: selectedAddress?.IdCatCteDirEnv,
    id_user: auth?.iduser,
    id_city: auth?.idcity,
    totalPayment: totalPayment,
    comentario:comentario
  });
  

  const compareFunction = async () => {

    //Select de ciudades change
    if(citySelected!==null){
      let json={"id_city_actual":auth?.idcity,"id_city_changed":citySelected,lst_products:products}
      
      const response = await getComparePriceCart(json);
      try {
        if(response.status==200){
          setForm({
            ...form,
            lst_products: response.result,
            totalPayment:response.total
          });

          setNewPayment(response.total);
          console.log((response));
          
        }else{
          
          setForm({
            ...form,
            lst_products: [],
            totalPayment:0
          });
          setNewPayment(0);
          Platform.OS === 'android'?
          ToastAndroid.show(response.result+" ¡Contacte al Administrador!", ToastAndroid.LONG):
          Toast.show(response.result+" ¡Contacte al Administrador!", { position: Toast.positions.CENTER, });
          
        } 
      } catch (error) {
        console.log(error);

      }
      
    }else{
      setForm({
        ...form,
        lst_products: products
      });
    }
  }

  //Se activa cuando cambia de direccion de envio
  useEffect(() => {
    setForm({
      ...form,
      id_address: selectedAddress?.IdCatCteDirEnv,
    });
  }, [selectedAddress]);

  //Cuando cambia el comentario
  useEffect(() => {
    setForm({
      ...form,
      comentario: comentario
    });
  }, [comentario]);


  //Se activa cuando hay un cambio en el status del producto 
  //ESTA MADRE ACTUALIZA TODO EL BISSNES
  useEffect(() => {
    (async () => {
    if(products?.length==0){
      setForm({
        ...form,
        lst_products: [],
        totalPayment:0
      });
    }else{
      setForm({
        ...form,
        lst_products: products,
        
      });
    }
    //Funcion del select
    compareFunction();
   
  })();
  }, [products,citySelected]);

  //Se activa cuando hay un cambio en el totalPayment
  useEffect(() => {
    setForm({
      ...form,
      totalPayment: totalPayment+parseInt(costoEnvio)
    });
    
    if(totalPayment==null){
      
      setNewPayment(0)
    }else{
      
      setNewPayment(totalPayment+parseInt(costoEnvio))
    }
    //console.log("useefectTotal: "+totalPayment);
    
    
  }, [totalPayment]);

  //Formulario enviado
  const onChange = (value: string, field: string) => {
    setForm({
      ...form,
      [field]: value,
    });
  };

  //Validar formulario
  const validateForm = () => {
    let msg = "";
    if(products?.length==0){
      Platform.OS === 'android'?
      ToastAndroid.show("Agregue productos al carrito para avanzar con el pago", ToastAndroid.LONG):
      Toast.show("Agregue productos al carrito para avanzar con el pago", { position: Toast.positions.CENTER, }); 
    }
    else if (
      form.nombre_tarjeta == ""
        ? (msg = "nombre tarjeta")
        : form.numero_tarjeta == ""
        ? (msg = "numero de tarjeta")
        : form.mes == ""
        ? (msg = "mes")
        : form.año == ""
        ? (msg = "año")
        : form.cvv == ""
        ? (msg = "cvv/cvc")
        : ""
    ) {
      Platform.OS === 'android'?
      ToastAndroid.show("El campo " + msg + " esta vacio", ToastAndroid.LONG):
      Toast.show("El campo " + msg + " esta vacio", { position: Toast.positions.CENTER, }); 
    } else {
      //Si no hay campos vacios hace el registro
      registrarCarrito();
      setModalVisible(false);
    }
  };

  const registrarCarrito = async () => {
    try {
      const response = await registerCart(form);
      console.log(response);
      
      if (response.status == 200) {
        /*Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); */
        //Redirect al webview
        navigation.navigate("WebViewPayment" as never,{response})
        
      } else {
        Platform.OS === 'android'?
        ToastAndroid.show(response.result, ToastAndroid.LONG):
        Toast.show(response.result, { position: Toast.positions.CENTER, }); 
      }
      //Borra el carrito del localstorage
      deleteAllCart();
    } catch (error) {
      console.log(error);
    }
  };

  //Valida si la ciudad seleccionada coincide con la de la direccion de envio
  const validateAddressCity = async () => {
    try {

      const response = await validateAddress(form.id_address,form.lst_products[0]?.ciudad);
      if (response.result) {
        //setModalVisible(true)
        modalPayment();
        //navigation.navigate("WebViewPayment" as never,{form})
        
      }else{
        Platform.OS === 'android'?
        ToastAndroid.show(response.msg, ToastAndroid.LONG):
        Toast.show(response.msg, { position: Toast.positions.CENTER, });
     
      }
      
    } catch (error) {
      console.log(error);
      
    }
  }

  const modalPayment = () => {
    Alert.alert(
      "Ir al pago",
      "¿Su carrito se borrará al avanzar,está seguro de que quieres pagar?",
      [
        {
          text: "No",
        },
        { text: "Si", onPress: registrarCarrito },
      ],
      { cancelable: false }
    );
  };



  
  return (
    <View style={styles.container}>
      {/*Boton modal*/ }
      <Button
        style={{
          width: "90%",
          marginLeft: "5%",
          marginRight: "5%",
          borderRadius: 15,
          backgroundColor: colores.primary,
        }}
        disabled={products?.length == 0 || form.lst_products?.length == 0}
        icon="credit-card-outline"
        mode="contained"
        uppercase={false}
        //onPress={() => setModalVisible(true)}
        onPress={() => validateAddressCity()}
      >
        <Text style={{ color: "white" }}>
          Realizar pago {newPayment && `$${newPayment}`}
        </Text>
      </Button>

      {/**Modal Payment */}
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredViewForm}>
            <View style={styles.modalView}>
              <Text style={styles.containerTitle}>Forma de pago</Text>
              <View style={styles.modalForm}>
                <TextInput
                  label={"Nombre de la tarjeta"}
                  style={styles.input}
                  mode="outlined"
                  onChangeText={(value) => onChange(value, "nombre_tarjeta")}
                  theme={{ colors: { primary: colores.blue }, roundness: 15 }}
                />
              </View>

              <View style={styles.modalForm}>
                <TextInput
                  label={"Número de la tarjeta"}
                  style={styles.input}
                  mode="outlined"
                  keyboardType="number-pad"
                  onChangeText={(value) => onChange(value, "numero_tarjeta")}
                  theme={{ colors: { primary: colores.blue }, roundness: 15 }}
                />
              </View>

              <View style={styles.containerInputs}>
                <View style={styles.containerMonthYearInputs}>
                  <TextInput
                    label={"Mes"}
                    style={styles.inputData}
                    mode="outlined"
                    keyboardType="number-pad"
                    onChangeText={(value) => onChange(value, "mes")}
                    theme={{ colors: { primary: colores.blue }, roundness: 15 }}
                  />
                  <TextInput
                    label={"Año"}
                    style={styles.inputData}
                    mode="outlined"
                    keyboardType="number-pad"
                    onChangeText={(value) => onChange(value, "año")}
                    theme={{ colors: { primary: colores.blue }, roundness: 15 }}
                  />
                </View>
                <TextInput
                  label={"Cvv"}
                  style={styles.inputCvv}
                  mode="outlined"
                  keyboardType="number-pad"
                  onChangeText={(value) => onChange(value, "cvv")}
                  theme={{ colors: { primary: colores.blue }, roundness: 15 }}
                />
              </View>

              <View style={styles.btnRowModal}>
                <Button
                  style={{
                    marginTop: 10,
                    width: "100%",
                    height:"90%",
                    marginLeft: "5%",
                    marginRight: "5%",
                    borderRadius: 15,
                    backgroundColor: colores.blue,
                  }}
                  icon="check-circle"
                  mode="contained"
                  uppercase={false}
                  onPress={() => modalPayment()}
                >
                  <Text style={{ color: "white" }}>Aceptar</Text>
                </Button>
                
                <Button
                  style={{
                    marginTop: 10,
                    width: "100%",
                    height:"90%",
                    marginLeft: "5%",
                    marginRight: "5%",
                    borderRadius: 15,
                    backgroundColor: colores.danger,
                  }}
                  icon="backspace-reverse"
                  mode="contained"
                  uppercase={false}
                  onPress={() => setModalVisible(false)}
                >
                  <Text style={{ color: "white" }}>Cancelar</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: Dimensions.get("window").height*0.08,
  },
  containerTitle: {
    paddingBottom: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
  input: {
    marginHorizontal: 5,
    marginBottom: 10,
    width: "115%",
  },
  containerInputs: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 20,
  },
  containerMonthYearInputs: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  inputData: {
    width: 90,
    marginRight: 10,
  },
  inputCvv: {
    width: 90,
  },
  btnContent: {
    paddingVertical: 4,
    backgroundColor: colores.primary,
  },
  btnText: {
    fontSize: 16,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: colores.blue,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  centeredViewForm: {
    flex: 1,
    justifyContent: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 20,
    elevation: 100,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 18,
  },
  modalForm: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnRowModal: {
    flexDirection: "row",
    width: "50%",
    justifyContent: "center",
  },
});
