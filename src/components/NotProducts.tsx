import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { colores } from "../theme/styles";
import { IconButton } from "react-native-paper";

export default function NotProducts() {
  return (
    <View style={styles.container}>
      <IconButton icon="pig-variant" color={colores.primary} size={60} style={styles.iconFlex}/>
      <Text style={styles.title}>No esta disponible la venta en linea para esta ciudad</Text>
    </View>
    
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection: 'column',
    alignItems:'center'
  },
  title: {
    flex:1,
    fontSize: 14,
    fontWeight: "bold",
    alignSelf:"center",
    textAlign:'center',
    color:colores.primary
  },
  costo:{
    fontSize: 15,
    fontWeight: "bold",
    alignSelf:"center",
    color:colores.primary
  },
  text:{
    color:colores.blue
  },
  titleNotCart: {
    flex:1,
    alignSelf:"center",
    marginTop:"50%",
    color:colores.primary
  },
  iconFlex:{
    alignSelf:"center"
  }
});
