import React from "react";
import { StyleSheet } from "react-native";
import { ActivityIndicator, Text } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { colores } from "../theme/styles";

export const Loading = () => {

  return (
    <SafeAreaView style={styles.container}>
      <ActivityIndicator size="small" color={colores.primary} style={styles.loading}/>
      <Text style={styles.title}>Cargando...</Text>
    </SafeAreaView>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems:"center"
  },
  loading:{
    marginBottom:10
  },
  title:{
    fontSize:16,
    textAlign:"center",
    color:colores.primary
  }
});