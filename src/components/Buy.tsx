import { View, Text, StyleSheet, ToastAndroid, Platform } from "react-native";
import React, { useEffect, useState } from "react";
import { Button } from "react-native-paper";
import { colores } from "../theme/styles";
import useAuth from "../hooks/useAuth";
import { addProductCartApi, getProductCartApi } from "../api/cart";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-root-toast";
import { getUserById } from "../properties/services";

export default function Buy(props:any) {
    const {product,quantity, tipo,precio}=props;
    const { auth } = useAuth();
    const navigation = useNavigation();
    const [loading, setLoading] = useState(false);
    const [userData, setUserData] = useState<any>();
    const idusuario=auth?.iduser;
    

  useEffect(() => {
    (async () =>{
      const response = await getUserById(idusuario);
      setUserData(response);
      setLoading(false);
    })()
  }, []);

    const validateForm=()=>{
      if(userData.username=='kowi'){
        Platform.OS === 'android'?
        ToastAndroid.show("Registrate para poder agregar al carrito", ToastAndroid.LONG):
        Toast.show("Registrate para poder agregar al carrito", { position: Toast.positions.CENTER, }); 
      }
      else if(quantity>10){
        Platform.OS === 'android'?
        ToastAndroid.show("La cantidad por producto tiene como maximo 10 unidades", ToastAndroid.LONG):
        Toast.show("La cantidad por producto tiene como maximo 10 unidades", { position: Toast.positions.CENTER, }); 

      }
      else if(quantity<=0){
        Platform.OS === 'android'?
        ToastAndroid.show("La cantidad debe ser mayor a 0 para avanzar", ToastAndroid.LONG):
        Toast.show("La cantidad debe ser mayor a 0 para avanzar", { position: Toast.positions.CENTER, }); 
      }
      else if(tipo=="Pieza" && quantity%1!=0){
        Platform.OS === 'android'?
        ToastAndroid.show("Solo admite cantidades enteras en piezas", ToastAndroid.LONG):
        Toast.show("Solo admite cantidades enteras en piezas", { position: Toast.positions.CENTER, }); 
      }
      else if(quantity>parseInt(product.Stock)){
        Platform.OS === 'android'?
        ToastAndroid.show("La cantidad que intentas pedir es mayor al stock actual", ToastAndroid.LONG):
        Toast.show("La cantidad que intentas pedir es mayor al stock actual", { position: Toast.positions.CENTER, }); 
      }
      else if(precio<=0 || precio==null){
        Platform.OS === 'android'?
        ToastAndroid.show("No se pueden agregar productos con precio 0", ToastAndroid.LONG):
        Toast.show("No se pueden agregar productos con precio 0", { position: Toast.positions.CENTER, }); 
      }
      else{
        addProductCart();
      }
    }

    const addProductCart=async ()=>{

      const objCarrito = await getProductCartApi(auth?.iduser);

      let countMax=0
      for (let i = 0; i < objCarrito.length; i++) {
        if(product.IdCatArticulos==objCarrito[i].idProduct){
          countMax=parseFloat(quantity)+parseFloat(objCarrito[i].quantity);
        }
      }

      if(countMax>10){
        Platform.OS === 'android'?
        ToastAndroid.show("Ya tienes este producto agregado en el carrito, no debe de superar los 10Kg/Pz", ToastAndroid.LONG):
        Toast.show("Ya tienes este producto agregado en el carrito, no debe de superar los 10Kg/Pz", { position: Toast.positions.CENTER, }); 
      }
      else{
        
        const response= await addProductCartApi(parseInt(product.IdCatArticulos),parseFloat(quantity),tipo,auth?.iduser,parseFloat(precio))
        
        if(response){
          navigation.pop();
          Platform.OS === 'android'?
          ToastAndroid.show("Producto añadido al carrito", ToastAndroid.LONG):
          Toast.show("Producto añadido al carrito", { position: Toast.positions.CENTER, }); 
          
        }else{
          Platform.OS === 'android'?
          ToastAndroid.show("Error añadir el producto al carrito", ToastAndroid.LONG):
          Toast.show("Error añadir el producto al carrito", { position: Toast.positions.CENTER, }); 
      }
      }
        
    }

  return (
    <View>
      <Button
        style={styles.button}
        icon="cart-plus"
        mode="contained"
        uppercase={false}
        onPress={() => validateForm()}
      >
        <Text> Agregar al carrito </Text>
      </Button>
    </View>
  );
}

const styles= StyleSheet.create({
    button:{
        width: "100%",
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor: colores.blue,
      }
})
