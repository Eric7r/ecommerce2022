import React from "react";
import { StyleSheet } from "react-native";
import { Text, View } from "react-native";
import { Button } from "react-native-paper";
import { colores } from "../theme/styles";



export const Favorites = (props:any) => {

    const {product}=props;

    const addFavorites=()=>{
        console.log("Agregado a favoritos")
        console.log(product.Descripcion)
    }

  return (
    <View>
      <Button
        style={styles.btnContent}
        icon="heart"
        mode="contained"
        uppercase={false}
        onPress={() => addFavorites()}
      >
        <Text> Agregar a favoritos</Text>
      </Button>
      
    </View>
  );
};

const styles = StyleSheet.create({
  btnContent: {
    backgroundColor: colores.blue,
    borderRadius: 15,
  },
});
