import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { AccountScreen } from "../screens/AccountScreen";
import AddAddressScreen from "../screens/AddAddressScreen";
import AddressScreen from "../screens/AddressScreen";
import DudasScreen from "../screens/DudasScreen";
import OrderScreen from "../screens/OrderScreen";
import RecetasScreen from "../screens/RecetasScreen";
import { colores } from "../theme/styles";
import FacturaScreen from "../screens/FacturaScreen";

const Stack = createStackNavigator();

export const AccountStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="account"
        component={AccountScreen}
        options={{
          title: "Cuenta",
          headerShown: false,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="address"
        component={AddressScreen}
        options={{
          title: "Mis Direcciones",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="addaddress"
        component={AddAddressScreen}
        options={{
          title: "Agregar dirección",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="orders"
        component={OrderScreen}
        options={{
          title: "Mis pedidos",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="dudas"
        component={DudasScreen}
        options={{
          title: "Dudas y aclaraciones",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="recetas"
        component={RecetasScreen}
        options={{
          title: "Recetas y preparaciones",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
      <Stack.Screen
        name="factura"
        component={FacturaScreen}
        options={{
          title: "Facturar",
          headerShown: true,
          headerStyle: { backgroundColor: colores.black },
          headerTintColor: colores.white,
        }}
      />
    </Stack.Navigator>
  );
};
