import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native'
import React from 'react';
import { AccountStack } from './AccountStack';
import { ProductStack } from './ProductStack';
import { CartStack } from './CartStack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


const Tab = createMaterialBottomTabNavigator();

export const NavigationStack = () => {
  
  return (
    <NavigationContainer>
        <Tab.Navigator 
        barStyle={{backgroundColor:'rgba(0, 0, 0, 0.75)',borderWidth:0, position:'absolute'}}>
            <Tab.Screen name="Home" component={ProductStack} options={{title:"Inicio",tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="home-outline" color={color} size={26} />
          )}}/>
            <Tab.Screen name="Cart" component={CartStack} options={{title:"Carrito", tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="cart-variant" color={color} size={26} />
          )}} />
            <Tab.Screen name="Account" component={AccountStack} options={{title:"Cuenta", tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="book-account" color={color} size={26} />
          )}}/>
        </Tab.Navigator>
    </NavigationContainer>
  )
}

