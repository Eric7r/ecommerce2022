import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { DetailProduct } from "../screens/DetailProduct";
import { HomeScreen } from "../screens/HomeScreen";
import { SearchProduct } from "../screens/SearchProduct";
import { colores } from "../theme/styles";

const Stack = createStackNavigator();

export const ProductStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor:colores.white,
        headerStyle:{backgroundColor:colores.black},
        cardStyle:{
          backgroundColor:colores.white
        }
      }}
    >
        <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown:false}}
        />
        <Stack.Screen
        name="DetailProduct"
        component={DetailProduct}
        options={{headerShown:false}}
        />
        <Stack.Screen
        name="SearchProduct"
        component={SearchProduct}
        options={{headerShown:false}}
        />
    </Stack.Navigator>
  );
};
