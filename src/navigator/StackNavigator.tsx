import { createStackNavigator } from '@react-navigation/stack';
import React, { useContext } from 'react'
import { ProductResponse } from '../interfaces/productsInterface';
import { DetailScreen } from '../screens/DetailScreen';
import { HomeScreen } from '../screens/HomeScreen';
import { LoginScreen } from '../screens/LoginScreen';
import RecoveryScreen from '../screens/RecoveryScreen';
import { RegisterScreen } from '../screens/RegisterScreen';
import FacturaScreen from '../screens/FacturaScreen';

export type RootStackParams={
  LoginScreen:undefined;
  HomeScreen:undefined;
  DetailScreen:ProductResponse;
  RegisterScreen:undefined;
  FacturaScreen:undefined;
}

const Stack = createStackNavigator<RootStackParams>();

export const StackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown:false,
        cardStyle:{
          backgroundColor:'white'
        }
      }}
    >
      <Stack.Screen name="LoginScreen"     component={LoginScreen} />
      <Stack.Screen name="HomeScreen"      component={HomeScreen} />
      <Stack.Screen name="DetailScreen"    component={DetailScreen} />
      <Stack.Screen name="RegisterScreen"  component={RegisterScreen} />
      <Stack.Screen name="FacturaScreen"   component={FacturaScreen} />
    </Stack.Navigator>
  )
}
