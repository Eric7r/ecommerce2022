import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import WebViewPayment from "../components/WebViewPayment";
import AddAddressScreen from "../screens/AddAddressScreen";
import { CartScreen } from "../screens/CartScreen";
import { colores } from "../theme/styles";

const Stack = createStackNavigator();

export const CartStack = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen
        name="cart"
        component={CartScreen}
        options={{title:"Carrito",headerShown:false}}
        />
        <Stack.Screen
        name="addaddress"
        component={AddAddressScreen}
        options={{title:"Agregar dirección",headerShown:true,headerStyle: {backgroundColor: colores.black},headerTintColor: colores.white}}
        />
        <Stack.Screen
        name="WebViewPayment"
        component={WebViewPayment}
        options={{title:"Apartado de pago",headerShown:true,headerStyle: {backgroundColor: colores.black},headerTintColor: colores.white}}
        />
    </Stack.Navigator>
  );
};
